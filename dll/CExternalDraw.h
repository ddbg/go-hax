/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dwmapi.lib")

#include <Windows.h>
#include <stdio.h>
#include <Dwmapi.h>
#include <d3d9.h>
#include <D3dx9core.h>

#include "CUtils.h"

/// <summary>
/// A generic window overlay.
/// </summary>
class CExternalDraw
{
public:
	CExternalDraw(PCHAR pszClassName, PCHAR pszWindowName, int nPosX, int nPosY, int nWidth, int nHeight, PVOID pRenderFunction);
	~CExternalDraw();

	LRESULT WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
	DWORD WindowThread(LPVOID lpThreadParameter);

	void BeginScene(bool bClear = true);
	void EndScene(bool bPresent = true);
	void DrawText(int nPosX, int nPosY, DWORD dwColor, PCHAR pszFormat, ...);
	void DrawLine(int nStartX, int nStartY, int nEndX, int nEndY, int nThickness, DWORD dwColor);
	void DrawRectangle(int nPosX, int nPosY, int nWidth, int nHeight, int nThickness, DWORD dwColor);

private:
	HANDLE m_hThread;
	HWND m_hWnd;
	PCHAR m_pszClassName;
	PCHAR m_pszWindowName;
	int m_nPosX;
	int m_nPosY;
	int m_nWidth;
	int m_nHeight;

	typedef void(__cdecl* RenderFunction)(void);
	RenderFunction m_pRenderFunction;

	PDIRECT3DDEVICE9EX m_pD3DDevice9;
	PDIRECT3D9EX m_pD3D9;
	LPD3DXFONT m_pD3DXFont;
	LPD3DXLINE m_pD3DXLine;

	/// <summary>
	/// A static-dummy thread routine that passes execution to the class thread routine.
	/// </summary>
	/// <param name="lpThreadParameter">The thread parameter.</param>
	/// <returns>If successful, this method should never return.</returns>
	static DWORD WINAPI DummyThread(LPVOID lpThreadParameter)
	{
		CExternalDraw* pExternalDraw = (CExternalDraw*)lpThreadParameter;
		return pExternalDraw->WindowThread(lpThreadParameter);
	}

	/// <summary>
	/// A static-dummy window procedure that passes execution to the class window procedure.
	/// </summary>
	/// <param name="hWnd">A handle to the window.</param>
	/// <param name="uMessage">The message.</param>
	/// <param name="wParam">Additional message information.</param>
	/// <param name="lParam">Additional message information.</param>
	/// <returns>Depends on the message sent.</returns>
	static LRESULT WINAPI DummyProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
	{
		if (uMessage == WM_CREATE) // Preprocess the WM_CREATE message.
		{
			MARGINS margin;			
			margin.cxLeftWidth = 0;
			margin.cyTopHeight = 0;
			margin.cxRightWidth = -1;
			margin.cyBottomHeight = -1;

			// Extends the window frame into the client area.
			DwmExtendFrameIntoClientArea(hWnd, &margin);
			return 0;
		}
		else
		{
			CExternalDraw* pExternalDraw = (CExternalDraw*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
			if (pExternalDraw)
				return pExternalDraw->WindowProc(hWnd, uMessage, wParam, lParam);
			else
				return DefWindowProc(hWnd, uMessage, wParam, lParam);
		}
	}
};