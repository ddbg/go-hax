/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CSettings.h"

/// <summary>
/// Initializes a new instance of the <see cref="CSettings"/> class.
/// </summary>
/// <param name="hLibrary">The loaded DLL module.</param>
CSettings::CSettings(HMODULE hLibrary) : m_hFile(INVALID_HANDLE_VALUE)
{
	ZeroMemory(m_szFileName, MAX_PATH);

	// Get the path of the loaded library.
	if (!GetModuleFileName(hLibrary, m_szFileName, MAX_PATH))
	{
		MessageBox(0, "Failed to locate path of loaded library. Using default config settings.", "[go]hax", MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	// Get the extension of the file.
	PCHAR pszExtension = strrchr(m_szFileName, '.');
	int nExtensionLength = strlen(pszExtension) + 1;
	if (nExtensionLength != 5)
	{
		MessageBox(0, "Invalid extension of loaded library. Using default config settings.", "[go]hax", MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	// Overwrite the extension (probably .dll) with .ini.
	strcpy_s(pszExtension, nExtensionLength, ".ini");

	// Try to open this file.
	m_hFile = CreateFile(m_szFileName, GENERIC_ALL, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		MessageBox(0, "Could not open settings (.INI) file for read. Using default config settings.", "[go]hax", MB_OK | MB_ICONEXCLAMATION);
		return;
	}
}

/// <summary>
/// Finalizes an instance of the <see cref="CSettings"/> class.
/// </summary>
CSettings::~CSettings()
{
	if (m_hFile != INVALID_HANDLE_VALUE)
		CloseHandle(m_hFile);
}

/// <summary>
/// Gets settings from the INI file, if applicable, or uses defaults.
/// </summary>
void CSettings::Get()
{
	TriggerBot.State = GetPrivateProfileInt("TriggerBot", "State", 0, m_szFileName);
	TriggerBot.Scale = GetPrivateProfileInt("TriggerBot", "Scale", 0, m_szFileName);

	FontESP.Enabled = GetPrivateProfileInt("FontESP", "Enabled", 1, m_szFileName);
	FontESP.ShowNames = GetPrivateProfileInt("FontESP", "ShowNames", 1, m_szFileName);
	FontESP.ShowWeapon = GetPrivateProfileInt("FontESP", "ShowWeapon", 1, m_szFileName);
	FontESP.ShowHealth = GetPrivateProfileInt("FontESP", "ShowHealth", 1, m_szFileName);
	FontESP.EnemyAlpha = GetPrivateProfileInt("FontESP", "EnemyAlpha", 255, m_szFileName);
	FontESP.AllyAlpha = GetPrivateProfileInt("FontESP", "AllyAlpha", 255, m_szFileName);
	FontESP.EnemyRed = GetPrivateProfileInt("FontESP", "EnemyRed", 255, m_szFileName);
	FontESP.EnemyGreen = GetPrivateProfileInt("FontESP", "EnemyGreen", 0, m_szFileName);
	FontESP.EnemyBlue = GetPrivateProfileInt("FontESP", "EnemyBlue", 0, m_szFileName);
	FontESP.AllyRed = GetPrivateProfileInt("FontESP", "AllyRed", 255, m_szFileName);
	FontESP.AllyGreen = GetPrivateProfileInt("FontESP", "AllyGreen", 0, m_szFileName);
	FontESP.AllyBlue = GetPrivateProfileInt("FontESP", "AllyBlue", 0, m_szFileName);

	BoneESP.Enabled = GetPrivateProfileInt("BoneESP", "Enabled", 1, m_szFileName);
	BoneESP.EnemyAlpha = GetPrivateProfileInt("BoneESP", "EnemyAlpha", 255, m_szFileName);
	BoneESP.AllyAlpha = GetPrivateProfileInt("BoneESP", "AllyAlpha", 255, m_szFileName);
	BoneESP.EnemyThickness = GetPrivateProfileInt("BoneESP", "EnemyThickness", 1, m_szFileName);
	BoneESP.AllyThickness = GetPrivateProfileInt("BoneESP", "AllyThickness", 1, m_szFileName);
	BoneESP.EnemyRed = GetPrivateProfileInt("BoneESP", "EnemyRed", 255, m_szFileName);
	BoneESP.EnemyGreen = GetPrivateProfileInt("BoneESP", "EnemyGreen", 0, m_szFileName);
	BoneESP.EnemyBlue = GetPrivateProfileInt("BoneESP", "EnemyBlue", 0, m_szFileName);
	BoneESP.AllyRed = GetPrivateProfileInt("BoneESP", "AllyRed", 255, m_szFileName);
	BoneESP.AllyGreen = GetPrivateProfileInt("BoneESP", "AllyGreen", 0, m_szFileName);
	BoneESP.AllyBlue = GetPrivateProfileInt("BoneESP", "AllyBlue", 0, m_szFileName);

	BoxESP.Enabled = GetPrivateProfileInt("BoxESP", "Enabled", 1, m_szFileName);
	BoxESP.EnemyAlpha = GetPrivateProfileInt("BoxESP", "EnemyAlpha", 255, m_szFileName);
	BoxESP.AllyAlpha = GetPrivateProfileInt("BoxESP", "AllyAlpha", 255, m_szFileName);
	BoxESP.EnemyThickness = GetPrivateProfileInt("BoxESP", "EnemyThickness", 1, m_szFileName);
	BoxESP.AllyThickness = GetPrivateProfileInt("BoxESP", "AllyThickness", 1, m_szFileName);
	BoxESP.EnemyRed = GetPrivateProfileInt("BoxESP", "EnemyRed", 255, m_szFileName);
	BoxESP.EnemyGreen = GetPrivateProfileInt("BoxESP", "EnemyGreen", 0, m_szFileName);
	BoxESP.EnemyBlue = GetPrivateProfileInt("BoxESP", "EnemyBlue", 0, m_szFileName);
	BoxESP.AllyRed = GetPrivateProfileInt("BoxESP", "AllyRed", 255, m_szFileName);
	BoxESP.AllyGreen = GetPrivateProfileInt("BoxESP", "AllyGreen", 0, m_szFileName);
	BoxESP.AllyBlue = GetPrivateProfileInt("BoxESP", "AllyBlue", 0, m_szFileName);
}