/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <Windows.h>
#include <d3d9.h>
#include <D3dx9core.h>

#include "sdk/source_sdk.h"
#include "sdk/gametrace.h"

/// <summary>
/// Interface to Aimbot related functionality.
/// </summary>
namespace CAimbot
{
	bool Trigger(IClientEntity *pLocalEntity, bool bHeadOnly, int nScale);
	void Shoot();
}