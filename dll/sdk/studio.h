/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <d3d9.h>
#include <D3dx9core.h>

// intersection boxes
struct mstudiobbox_t
{
	int					bone;
	int					group;				// intersection group
	D3DXVECTOR3			bbmin;				// bounding box
	D3DXVECTOR3			bbmax;
	int					szhitboxnameindex;	// offset to the name of the hitbox.
	int					unused[8];

	const char* pszHitboxName()
	{
		if (szhitboxnameindex == 0)
			return "";

		return ((const char*)this) + szhitboxnameindex;
	}

	mstudiobbox_t() {}

private:
	// No copy constructors allowed
	mstudiobbox_t(const mstudiobbox_t& vOther);
};

struct mstudiohitboxset_t
{
	int					sznameindex;
	inline char * const	pszName(void) const { return ((char *)this) + sznameindex; }
	int					numhitboxes;
	int					hitboxindex;
	inline mstudiobbox_t *pHitbox(int i) const { return (mstudiobbox_t *)(((byte *)this) + hitboxindex) + i; };
};

struct studiohdr_t
{
	int					id;
	int					version;

	int					checksum;		// this has to be the same in the phy and vtx files to load!

	inline const char *	pszName(void) const { /*if (studiohdr2index && pStudioHdr2()->pszName()) return pStudioHdr2()->pszName(); else*/ return name; }
	char				name[64];
	int					length;


	D3DXVECTOR3			eyeposition;	// ideal eye position

	D3DXVECTOR3			illumposition;	// illumination center

	D3DXVECTOR3			hull_min;		// ideal movement hull size
	D3DXVECTOR3			hull_max;

	D3DXVECTOR3			view_bbmin;		// clipping bounding box
	D3DXVECTOR3			view_bbmax;

	int					flags;

	int					numbones;			// bones
	int					boneindex;
	inline PVOID		pBone(int i) const { return NULL; };
	int					RemapSeqBone(int iSequence, int iLocalBone) const;	// maps local sequence bone to global bone
	int					RemapAnimBone(int iAnim, int iLocalBone) const;		// maps local animations bone to global bone

	int					numbonecontrollers;		// bone controllers
	int					bonecontrollerindex;
	inline PVOID		pBonecontroller(int i) const { return NULL; };

	int					numhitboxsets;
	int					hitboxsetindex;

	// Look up hitbox set by index
	mstudiohitboxset_t	*pHitboxSet(int i) const
	{
		return (mstudiohitboxset_t *)(((byte *)this) + hitboxsetindex) + i;
	};

	// Calls through to hitbox to determine size of specified set
	inline mstudiobbox_t *pHitbox(int i, int set) const
	{
		mstudiohitboxset_t const *s = pHitboxSet(set);
		if (!s)
			return NULL;

		return s->pHitbox(i);
	};

	// Calls through to set to get hitbox count for set
	inline int			iHitboxCount(int set) const
	{
		mstudiohitboxset_t const *s = pHitboxSet(set);
		if (!s)
			return 0;

		return s->numhitboxes;
	};

	// file local animations? and sequences
	//private:
	int					numlocalanim;			// animations/poses
	int					localanimindex;		// animation descriptions
	inline PVOID		pLocalAnimdesc(int i) const { if (i < 0 || i >= numlocalanim) i = 0; return /*(mstudioanimdesc_t *)(((byte *)this) + localanimindex) + i*/ NULL; };

	int					numlocalseq;				// sequences
	int					localseqindex;
	inline PVOID		pLocalSeqdesc(int i) const { if (i < 0 || i >= numlocalseq) i = 0; return /*(mstudioseqdesc_t *)(((byte *)this) + localseqindex) + i*/ NULL; };

	//public:
	bool				SequencesAvailable() const;
	int					GetNumSeq() const;
	PVOID				pAnimdesc(int i) const;
	PVOID				pSeqdesc(int i) const;
	int					iRelativeAnim(int baseseq, int relanim) const;	// maps seq local anim reference to global anim index
	int					iRelativeSeq(int baseseq, int relseq) const;		// maps seq local seq reference to global seq index

	//private:
	mutable int			activitylistversion;	// initialization flag - have the sequences been indexed?
	mutable int			eventsindexed;
	//public:
	int					GetSequenceActivity(int iSequence);
	void				SetSequenceActivity(int iSequence, int iActivity);
	int					GetActivityListVersion(void);
	void				SetActivityListVersion(int version) const;
	int					GetEventListVersion(void);
	void				SetEventListVersion(int version);

	// raw textures
	int					numtextures;
	int					textureindex;
	inline PVOID		pTexture(int i) const { return NULL; };


	// raw textures search paths
	int					numcdtextures;
	int					cdtextureindex;
	inline char			*pCdtexture(int i) const { return (((char *)this) + *((int *)(((byte *)this) + cdtextureindex) + i)); };

	// replaceable textures tables
	int					numskinref;
	int					numskinfamilies;
	int					skinindex;
	inline short		*pSkinref(int i) const { return (short *)(((byte *)this) + skinindex) + i; };

	int					numbodyparts;
	int					bodypartindex;
	inline PVOID		pBodypart(int i) const { return /*(mstudiobodyparts_t *)(((byte *)this) + bodypartindex) + i*/ NULL; };

	// queryable attachable points
	//private:
	int					numlocalattachments;
	int					localattachmentindex;
	inline PVOID		pLocalAttachment(int i) const { return NULL; };
	//public:
	int					GetNumAttachments(void) const;
	PVOID				pAttachment(int i) const;
	int					GetAttachmentBone(int i);
	// used on my tools in hlmv, not persistant
	void				SetAttachmentBone(int iAttachment, int iBone);

	// animation node to animation node transition graph
	//private:
	int					numlocalnodes;
	int					localnodeindex;
	int					localnodenameindex;
	inline char			*pszLocalNodeName(int iNode) const { return (((char *)this) + *((int *)(((byte *)this) + localnodenameindex) + iNode)); }
	inline byte			*pLocalTransition(int i) const { return (byte *)(((byte *)this) + localnodeindex) + i; };

	//public:
	int					EntryNode(int iSequence);
	int					ExitNode(int iSequence);
	char				*pszNodeName(int iNode);
	int					GetTransition(int iFrom, int iTo) const;

	int					numflexdesc;
	int					flexdescindex;
	inline PVOID		pFlexdesc(int i) const { return NULL; };

	int					numflexcontrollers;
	int					flexcontrollerindex;
	inline				PVOID *pFlexcontroller(int i) const { return NULL; };

	int					numflexrules;
	int					flexruleindex;
	inline				PVOID pFlexRule(int i) const { return NULL; };

	int					numikchains;
	int					ikchainindex;
	inline				PVOID pIKChain(int i) const { return NULL; };

	int					nummouths;
	int					mouthindex;
	inline				PVOID pMouth(int i) const { return NULL; };

	//private:
	int					numlocalposeparameters;
	int					localposeparamindex;
	inline				PVOID pLocalPoseParameter(int i) const { return NULL; };
	//public:
	int					GetNumPoseParameters(void) const;
	const				PVOID pPoseParameter(int i);
	int					GetSharedPoseParameter(int iSequence, int iLocalPose) const;

	int					surfacepropindex;
	inline char * const pszSurfaceProp(void) const { return ((char *)this) + surfacepropindex; }

	// Key values
	int					keyvalueindex;
	int					keyvaluesize;
	inline const char * KeyValueText(void) const { return keyvaluesize != 0 ? ((char *)this) + keyvalueindex : NULL; }

	int					numlocalikautoplaylocks;
	int					localikautoplaylockindex;
	inline				PVOID pLocalIKAutoplayLock(int i) const { return NULL; };

	int					GetNumIKAutoplayLocks(void) const;
	PVOID				pIKAutoplayLock(int i);
	int					CountAutoplaySequences() const;
	int					CopyAutoplaySequences(unsigned short *pOut, int outCount) const;
	int					GetAutoplayList(unsigned short **pOut) const;

	// The collision model mass that jay wanted
	float				mass;
	int					contents;

	// external animations, models, etc.
	int					numincludemodels;
	int					includemodelindex;
	PVOID				pModelGroup(int i) const { return NULL; };
	// implementation specific call to get a named model
	const studiohdr_t	*FindModel(void **cache, char const *modelname) const;

	// implementation specific back pointer to virtual data
	mutable void		*virtualModel;
	PVOID				GetVirtualModel(void) const;

	// for demand loaded animation blocks
	int					szanimblocknameindex;
	inline char * const pszAnimBlockName(void) const { return ((char *)this) + szanimblocknameindex; }
	int					numanimblocks;
	int					animblockindex;
	inline				PVOID pAnimBlock(int i) const { return NULL; };
	mutable void		*animblockModel;
	byte *				GetAnimBlock(int i) const;

	int					bonetablebynameindex;
	inline const byte	*GetBoneTableSortedByName() const { return (byte *)this + bonetablebynameindex; }

	// used by tools only that don't cache, but persist mdl's peer data
	// engine uses virtualModel to back link to cache pointers
	void				*pVertexBase;
	void				*pIndexBase;

	// if STUDIOHDR_FLAGS_CONSTANT_DIRECTIONAL_LIGHT_DOT is set,
	// this value is used to calculate directional components of lighting 
	// on static props
	byte				constdirectionallightdot;

	// set during load of mdl data to track *desired* lod configuration (not actual)
	// the *actual* clamped root lod is found in studiohwdata
	// this is stored here as a global store to ensure the staged loading matches the rendering
	byte				rootLOD;

	// set in the mdl data to specify that lod configuration should only allow first numAllowRootLODs
	// to be set as root LOD:
	//	numAllowedRootLODs = 0	means no restriction, any lod can be set as root lod.
	//	numAllowedRootLODs = N	means that lod0 - lod(N-1) can be set as root lod, but not lodN or lower.
	byte				numAllowedRootLODs;

	byte				unused[1];

	int					unused4; // zero out if version < 47

	int					numflexcontrollerui;
	int					flexcontrolleruiindex;
	PVOID				pFlexControllerUI(int i) const { return NULL; }

	int					unused3[2];

	// FIXME: Remove when we up the model version. Move all fields of studiohdr2_t into studiohdr_t.
	int					studiohdr2index;
	PVOID				pStudioHdr2() const { return (((byte *)this) + studiohdr2index); }

	// Src bone transforms are transformations that will convert .dmx or .smd-based animations into .mdl-based animations
	int					NumSrcBoneTransforms() const { return /*studiohdr2index ? pStudioHdr2()->numsrcbonetransform : */ 0; }
	const				PVOID SrcBoneTransform(int i) const { return NULL; }

	inline int			IllumPositionAttachmentIndex() const { return 0; }

	inline float		MaxEyeDeflection() const { return 0.866f; } // default to cos(30) if not set

	inline PVOID		pLinearBones() const { return NULL; }

	// NOTE: No room to add stuff? Up the .mdl file format version 
	// [and move all fields in studiohdr2_t into studiohdr_t and kill studiohdr2_t],
	// or add your stuff to studiohdr2_t. See NumSrcBoneTransforms/SrcBoneTransform for the pattern to use.
	int					unused2[1];

	studiohdr_t() {}

private:
	// No copy constructors allowed
	studiohdr_t(const studiohdr_t& vOther);
};