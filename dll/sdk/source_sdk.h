/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include "dt_recv2.h"
#include "studio.h"
#include <vector>

#pragma region SDK defines
#define CLIENT_DLL_INTERFACE_VERSION			"VClient016"
#define VENGINE_CLIENT_INTERFACE_VERSION		"VEngineClient014"
#define VENGINE_CLIENT_INTERFACE_VERSION_13		"VEngineClient013"
#define VCLIENTENTITYLIST_INTERFACE_VERSION		"VClientEntityList003"
#define VMODELINFO_CLIENT_INTERFACE_VERSION		"VModelInfoClient004"
#define INTERFACEVERSION_ENGINETRACE_CLIENT		"EngineTraceClient004"
#define STUDIO_RENDER_INTERFACE_VERSION			"VStudioRender026"
#define VGUI_SURFACE_INTERFACE_VERSION			"VGUI_Surface031"
#define VDEBUG_OVERLAY_INTERFACE_VERSION		"VDebugOverlay004"
// m_lifeState values
#define	LIFE_ALIVE				0 // alive
#define	LIFE_DYING				1 // playing death animation or still falling off of a ledge waiting to hit ground
#define	LIFE_DEAD				2 // dead. lying still.
#define LIFE_RESPAWNABLE		3
#define LIFE_DISCARDBODY		4
#pragma endregion

#pragma region SDK structs & types
typedef void* (*CreateInterfaceFn)(const char *pName, int *pReturnCode);

typedef float matrix3x4_t[3][4];

typedef struct player_info_s
{
	// unknown stuff in the beginning
	byte		Reserved0[16];

	// scoreboard information
	char			name[32];
	// local server user ID, unique while server is running
	int				userID;
	// global unique player identifier
	char			guid[32 + 1];
	// friends identification number
	long			friendsID;
	// friends name
	char			friendsName[32];
	// true, if player is a bot controlled by game.dll
	bool			fakeplayer;
	// true if player is the HLTV proxy
	bool			ishltv;
	// custom files CRC for this player
	unsigned int	customFiles[4];
	// this counter increases each time the server downloaded a new file
	unsigned char	filesDownloaded;

	// underflow is ok. overflow not. add garbage padding just in case.
	byte			Reserved1[1024];
} player_info_t;
#pragma endregion

#pragma region Helper structs & types
struct _NETVAR_OFFSETS
{
	int m_lifeState;
	int m_iHealth;
	int m_iTeamNum;
	int m_fFlags;
	int m_hOwnerEntity;
	int m_hActiveWeapon;
	int m_CurrentStage;
	int m_flFlashDuration;
	int m_flFlashMaxAlpha;
	int m_bIsScoped;
	int m_vecViewOffset;
	int m_nHitboxSet;
	int o_Util_TraceLine;
};

class RenderPlayer
{
public:
	D3DXVECTOR3 vecHead;
	D3DXVECTOR3 vecLeg;
	char szWeaponName[64];
	char szPlayerName[33 /* Maximum size of the name field in the player_info_s struct. */];
	int nHealth;
	int nTeamID;
	std::vector<D3DXVECTOR3> lstVecBoneStart;
	std::vector<D3DXVECTOR3> lstVecBoneEnd;
};

struct _RENDER_OBJECTS
{
	CRITICAL_SECTION cs;
	int LocalPlayerTeam;
	std::vector<RenderPlayer> Players;
};

extern _NETVAR_OFFSETS g_Offsets;
extern _RENDER_OBJECTS g_RenderObjects;
#pragma endregion

#pragma region Helper functions
inline void**& Vtable(void* inst, size_t offset = 0)
{
	return *reinterpret_cast<void***>((size_t)inst + offset);
}

inline const void** Vtable(const void* inst, size_t offset = 0)
{
	return *reinterpret_cast<const void***>((size_t)inst + offset);
}

template< typename Fn >
inline Fn Vcall(const void* inst, size_t index, size_t offset = 0)
{
	return reinterpret_cast<Fn>(Vtable(inst, offset)[index]);
}
#pragma endregion

#pragma region SDK classes
class IVModelInfo;
class ClientClass;
class IBaseClientDLL;
class C_BaseCombatWeapon;
class IClientEntityList;
class IClientEntity;
class IVEngineClient;
class IStudioRender;

class IVModelInfo
{
public:
	const char* GetModelName(PVOID pModel)
	{
		typedef const char* (__thiscall* _GetModelName)(PVOID, PVOID);
		return Vcall<_GetModelName>(this, 3)(this, pModel);
	}

	studiohdr_t * GetStudiomodel(PVOID pModel)
	{
		typedef studiohdr_t* (__thiscall* _GetStudiomodel)(PVOID, PVOID);
		return Vcall<_GetStudiomodel>(this, 30)(this, pModel);
	}
};

class ClientClass
{
public:
	const char* GetName(void)
	{
		return *(char**)((PUCHAR)this + 0x8);
	}

	RecvTable* GetTable()
	{
		return *(RecvTable**)((PUCHAR)this + 0xC);
	}

	ClientClass* NextClass()
	{
		return *(ClientClass**)((PUCHAR)this + 0x10);
	}

	int GetClassID(void)
	{
		return *(int*)((PUCHAR)this + 0x14);
	}
};

class IBaseClientDLL
{
public:
	ClientClass * GetAllClasses()
	{
		typedef ClientClass * (__thiscall* _GetAllClasses)(PVOID);
		return Vcall<_GetAllClasses>(this, 8)(this);
	}
};

class C_BaseCombatWeapon
{
public:
	PCHAR GetName()
	{
		typedef PCHAR (__thiscall* _GetName)(PVOID);
		return Vcall<_GetName>(this, 371)(this);
	}

	PCHAR GetPrintName()
	{
		typedef PCHAR (__thiscall* _GetPrintName)(PVOID);
		return Vcall<_GetPrintName>(this, 372)(this);
	}
};

class IClientEntityList
{
public:
	IClientEntity * GetClientEntity(int entnum)
	{
		typedef IClientEntity * (__thiscall* _GetClientEntity)(PVOID, INT);
		return Vcall<_GetClientEntity>(this, 3)(this, entnum);
	}

	IClientEntity * GetClientEntityFromHandle(HANDLE hEnt)
	{
		typedef IClientEntity* (__thiscall* _GetClientEntityFromHandle)(PVOID, HANDLE);
		return Vcall<_GetClientEntityFromHandle>(this, 4)(this, hEnt);
	}

	int NumberOfEntities(bool bIncludeNonNetworkable)
	{
		typedef int(__thiscall* _NumberOfEntities)(PVOID, bool);
		return Vcall<_NumberOfEntities>(this, 5)(this, bIncludeNonNetworkable);
	}

	int GetHighestEntityIndex()
	{
		typedef int(__thiscall* _GetHighestEntityIndex)(PVOID);
		return Vcall<_GetHighestEntityIndex>(this, 6)(this);
	}
};

class IClientEntity
{
public:
	int GetTeam(void)
	{
		return *(PINT)((PUCHAR)this + g_Offsets.m_iTeamNum);
	}

	byte GetLifeState(void)
	{
		return *(PBYTE)((PUCHAR)this + g_Offsets.m_lifeState);
	}

	int GetHealth(void)
	{
		return *(PINT)((PUCHAR)this + g_Offsets.m_iHealth);
	}

	bool IsScoped(void)
	{
		return *(bool*)((PUCHAR)this + g_Offsets.m_bIsScoped);
	}
	
	int GetFlags(void)
	{
		return *(PINT)((PUCHAR)this + g_Offsets.m_fFlags);
	}

	int GetHitboxSet()
	{
		return *(PINT)((PUCHAR)this + g_Offsets.m_nHitboxSet);
	}

	D3DXVECTOR3 GetEyePosition()
	{
		D3DXVECTOR3 vecViewOffset = *(D3DXVECTOR3*)((PUCHAR)this + g_Offsets.m_vecViewOffset);
		return (GetAbsOrigin() + vecViewOffset);
	}

	C_BaseCombatWeapon* GetActiveWeapon(IClientEntityList* pClientEntityList)
	{
		HANDLE hActiveWeapon = *(PHANDLE)((PUCHAR)this + g_Offsets.m_hActiveWeapon);
		if (!hActiveWeapon)
			return NULL;

		return (C_BaseCombatWeapon*)pClientEntityList->GetClientEntityFromHandle(hActiveWeapon);
	}

	D3DXVECTOR3& GetAbsOrigin()
	{
		typedef D3DXVECTOR3& (__thiscall* _GetAbsOrigin)(PVOID);
		return Vcall<_GetAbsOrigin>(this, 10)(this);
	}

	bool IsDormant()
	{
		PVOID pNetworkable = (PVOID)((PUCHAR)this + 0x8);
		if (!pNetworkable)
			return true;

		typedef bool(__thiscall* _IsDormant)(PVOID);
		return Vcall<_IsDormant>(pNetworkable, 9)(pNetworkable);
	}

	PVOID GetModel()
	{
		PVOID pRenderable = (PVOID)((PUCHAR)this + 0x4);
		if (!pRenderable)
			false;

		typedef PVOID (__thiscall* _GetModel)(PVOID);
		return Vcall<_GetModel>(pRenderable, 8)(pRenderable);
	}

	bool SetupBones(PVOID pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime)
	{	
		PVOID pRenderable = (PVOID)((PUCHAR)this + 0x4);
		if (!pRenderable)
			false;

		typedef bool(__thiscall* _SetupBones)(PVOID, PVOID, int, int, float);
		return Vcall<_SetupBones>(pRenderable, 13)(pRenderable, pBoneToWorldOut, nMaxBones, boneMask, currentTime); // This caused my crash.
	}
};

class IVEngineClient
{
public:
	bool Con_IsVisible(void)
	{
		typedef bool(__thiscall* _Con_IsVisible)(PVOID);
		return Vcall<_Con_IsVisible>(this, 11)(this);
	}

	int GetLocalPlayer()
	{
		typedef int(__thiscall* _GetLocalPlayer)(PVOID);
		return Vcall<_GetLocalPlayer>(this, 12)(this);
	}

	bool IsInGame()
	{
		typedef bool(__thiscall* _IsInGame)(PVOID);
		return Vcall<_IsInGame>(this, 26)(this);
	}

	bool IsConnected()
	{
		typedef bool(__thiscall* _IsConnected)(PVOID);
		return Vcall< _IsConnected >(this, 27)(this);
	}

	bool IsDrawingLoadingImage(void)
	{
		typedef bool(__thiscall* _IsDrawingLoadingImage)(PVOID);
		return Vcall<_IsDrawingLoadingImage>(this, 28)(this);
	}

	bool GetPlayerInfo(int ent_num, player_info_t *pinfo)
	{
		typedef bool(__thiscall* _GetPlayerInfo)(PVOID, int, player_info_t *);
		return Vcall<_GetPlayerInfo>(this, 8)(this, ent_num, pinfo);

	}

	float Time(void)
	{
		typedef float(__thiscall* _Time)(PVOID);
		return Vcall<_Time>(this, 14)(this);
	}

	void GetViewAngles(D3DXVECTOR3& va)
	{
		typedef void(__thiscall* _GetViewAngles)(PVOID, D3DXVECTOR3&);
		return Vcall<_GetViewAngles>(this, 18)(this, va);
	}

	void GetScreenSize(int& width, int& height)
	{
		typedef void(__thiscall* _GetScreenSize)(PVOID, int&, int&);
		return Vcall<_GetScreenSize>(this, 5)(this, width, height);
	}

	matrix3x4_t& WorldToScreenMatrix(void)
	{
		typedef matrix3x4_t& (__thiscall* _WorldToScreenMatrix)(PVOID);
		return Vcall<_WorldToScreenMatrix>(this, 37)(this);
	}


};

class IStudioRender
{
public:
	void BeginFrame()
	{
		typedef void(__thiscall* _BeginFrame)(PVOID);
		return Vcall<_BeginFrame>(this, 8)(this);
	}
};
#pragma endregion