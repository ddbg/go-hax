/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CAimbot.h"

extern IVEngineClient * g_pEngineClient;
extern IVModelInfo * g_pModelInfo;
extern HMODULE g_hClient;
extern _NETVAR_OFFSETS g_Offsets;

#define M_PI			3.14159265358979323846
#define M_PI_F		((float)(M_PI))	// Shouldn't collide with anything.
#define DEG2RAD( x  )  ( (float)(x) * (float)(M_PI_F / 180.f) )

namespace CAimbot
{
	/// <summary>
	/// Dot product of two vectors.
	/// </summary>
	/// <param name="v1">A vector.</param>
	/// <param name="v2">A vector.</param>
	/// <returns>The dot product of the two vectors.</returns>
	float DotProduct(const float  *v1, const float  *v2)
	{
		return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
	}

	/// <summary>
	/// Transforms a vector.
	/// </summary>
	/// <param name="in1">A vector.</param>
	/// <param name="in2">A vector.</param>
	/// <param name="out">[OUTPUT] Transformed vector.</param>
	void VectorTransform(D3DXVECTOR3 in1, matrix3x4_t in2, D3DXVECTOR3& out)
	{
		out[0] = DotProduct(in1, in2[0]) + in2[0][3];
		out[1] = DotProduct(in1, in2[1]) + in2[1][3];
		out[2] = DotProduct(in1, in2[2]) + in2[2][3];
	}

	/// <summary>
	/// Angles a vector.
	/// </summary>
	/// <param name="angles">The angles.</param>
	/// <param name="forward">[OUTPUT] The angled vector.</param>
	void AngleVectors(D3DXVECTOR3 angles, D3DXVECTOR3 *forward)
	{
		float    sp, sy, cp, cy;

		sy = sin(DEG2RAD(angles[1]));
		cy = cos(DEG2RAD(angles[1]));
		sp = sin(DEG2RAD(angles[0]));
		cp = cos(DEG2RAD(angles[0]));

		forward->x = cp*cy;
		forward->y = cp*sy;
		forward->z = -sp;
	}

	/// <summary>
	/// Computes the intersection of a ray with an oriented box (OBB).
	/// </summary>
	/// <param name="vecRayStart">The start vector.</param>
	/// <param name="vecRayDelta">The delta vector.</param>
	/// <param name="matOBBToWorld">The oriented box matrix.</param>
	/// <param name="vecOBBMins">The oriented box minimums.</param>
	/// <param name="vecOBBMaxs">The oriented box maximums.</param>
	/// <returns>Returns TRUE if there is an intersection and trace information.</returns>
	bool IntersectRayWithOBB(D3DXVECTOR3 vecRayStart, D3DXVECTOR3 vecRayDelta, matrix3x4_t matOBBToWorld, D3DXVECTOR3 vecOBBMins, D3DXVECTOR3 vecOBBMaxs)
	{
		// OPTIMIZE: Store this in the box instead of computing it here
		// compute center in local space
		D3DXVECTOR3 vecBoxExtents = (vecOBBMins + vecOBBMaxs) * 0.5;
		D3DXVECTOR3 vecBoxCenter;

		// transform to world space
		VectorTransform(vecBoxExtents, matOBBToWorld, vecBoxCenter);

		// calc extents from local center
		vecBoxExtents = vecOBBMaxs - vecBoxExtents;
		
		// OPTIMIZE: This is optimized for world space.  If the transform is fast enough, it may make more
		// sense to just xform and call UTIL_ClipToBox() instead.  MEASURE THIS.

		// save the extents of the ray along 
		D3DXVECTOR3 vecExtent, vecUExtent;
		D3DXVECTOR3 vecSegmentCenter = vecRayStart + vecRayDelta - vecBoxCenter;

		vecExtent.x = vecExtent.y = vecExtent.z = 0.0f;

		// check box axes for separation
		for (int j = 0; j < 3; j++)
		{
			vecExtent[j] = vecRayDelta.x * matOBBToWorld[0][j] + vecRayDelta.y * matOBBToWorld[1][j] + vecRayDelta.z * matOBBToWorld[2][j];
			vecUExtent[j] = fabsf(vecExtent[j]);
			float coord = vecSegmentCenter.x * matOBBToWorld[0][j] + vecSegmentCenter.y * matOBBToWorld[1][j] + vecSegmentCenter.z * matOBBToWorld[2][j];
			coord = fabsf(coord);

			if (coord >(vecBoxExtents[j] + vecUExtent[j]))
				return false;
		}

		// now check cross axes for separation
		float tmp, cextent;
		D3DXVECTOR3 vecCross;
		D3DXVec3Cross(&vecCross, &vecRayDelta, &vecSegmentCenter);

		cextent = vecCross.x * matOBBToWorld[0][0] + vecCross.y * matOBBToWorld[1][0] + vecCross.z * matOBBToWorld[2][0];
		cextent = fabsf(cextent);
		tmp = vecBoxExtents[1] * vecUExtent[2] + vecBoxExtents[2] * vecUExtent[1];
		if (cextent > tmp)
			return false;

		cextent = vecCross.x * matOBBToWorld[0][1] + vecCross.y * matOBBToWorld[1][1] + vecCross.z * matOBBToWorld[2][1];
		cextent = fabsf(cextent);
		tmp = vecBoxExtents[0] * vecUExtent[2] + vecBoxExtents[2] * vecUExtent[0];
		if (cextent > tmp)
			return false;

		cextent = vecCross.x * matOBBToWorld[0][2] + vecCross.y * matOBBToWorld[1][2] + vecCross.z * matOBBToWorld[2][2];
		cextent = fabsf(cextent);
		tmp = vecBoxExtents[0] * vecUExtent[1] + vecBoxExtents[1] * vecUExtent[0];
		if (cextent > tmp)
			return false;
	
		return true;
	}

	/// <summary>
	/// Checks to see if the local entity is targeting a shootable object.
	/// </summary>
	/// <param name="pLocalEntity">The local entity.</param>
	/// <param name="bHeadOnly">If set to <c>true</c>, ensures that the head hitbox is targetted.</param>
	/// <param name="nScale">Scales the hitbox to ensure better accuracy.</param>
	/// <returns>Returns TRUE if there is a shootable entity under the cursor that can be hit without going through other objects.</returns>
	bool Trigger(IClientEntity *pLocalEntity, bool bHeadOnly, int nScale)
	{
		// Setup our "look vector"
		D3DXVECTOR3 vecEyePosition = pLocalEntity->GetEyePosition();
		D3DXVECTOR3 vecViewAngle;

		g_pEngineClient->GetViewAngles(vecViewAngle);
		AngleVectors(vecViewAngle, &vecViewAngle);
		D3DXVec3Normalize(&vecViewAngle, &vecViewAngle);
		D3DXVec3Scale(&vecViewAngle, &vecViewAngle, 8192.f);
		
		D3DXVECTOR3 vecViewLocation = vecEyePosition + vecViewAngle;

		// Call traceline.
		DWORD dwTraceLine = (DWORD)((PUCHAR)g_hClient + g_Offsets.o_Util_TraceLine);
		
		/*	This code does not work because the calling convention for UTIL_Traceline does not conform to the MSVC compiler standards. 

			UTIL_Traceline has been compiled as a caller clean-up fastcall. The first 2 parameters
			are in ecx and edx and the rest is on stack. Unfortunately, MSVC's implementation of 
			fastcall is callee clean-up. 			
			
			typedef void(__fastcall* _UTIL_TraceLine)(PVOID vecAbsStart, PVOID vecAbsEnd, UINT mask, PVOID ignore, UINT collisionGroup, PVOID ptr);
			_UTIL_TraceLine UTIL_TraceLine = (_UTIL_TraceLine)dwTraceLine;
		
			UTIL_TraceLine(&Src, &Dst, 0x4600400B, pLocal, 0, &tr);
		*/

		CGameTrace trace;
		_asm
		{
			MOV EAX, pLocalEntity
			LEA ECX, trace
			PUSH ECX
			PUSH 0
			PUSH EAX
			PUSH 0x4600400B /* The appropriate mask that avoids collisions with non-penetrable objects and walls. */
			LEA EDX, vecViewLocation
			LEA ECX, vecEyePosition
			CALL dwTraceLine
			ADD ESP, 0x10
		}
		
		// If we don't hit a human we return.
		if (!(trace.hitgroup <= 10 && trace.hitgroup > 0))
			return false;

		if (bHeadOnly)
		{
			if (trace.hitbox != 11 && trace.hitbox != 20)
				return false;
		}

		IClientEntity* pTarget = (IClientEntity*)trace.m_pEnt;
		if (!pTarget)
			return false;

		// We only want to hit enemies.
		int nTargetTeam = pTarget->GetTeam();
		if (nTargetTeam == pLocalEntity->GetTeam())
			return false;
		
		// Don't triggerbot on non-CT and T targets.
		if (nTargetTeam != 2 && nTargetTeam != 3)
			return false;

		matrix3x4_t matrix[128];
		if (!pTarget->SetupBones(matrix, 128, 0x00000100, g_pEngineClient->Time()))
			return false;
		
		// Get our model studio stuff.
		PVOID model = pTarget->GetModel();
		if (!model)
			return false;

		studiohdr_t* pStudioHdr = g_pModelInfo->GetStudiomodel(model);
		if (!pStudioHdr)
			return false;

		int nHitboxSet = pTarget->GetHitboxSet();
		mstudiohitboxset_t* pHitboxSet = pStudioHdr->pHitboxSet(nHitboxSet);
		if (!pHitboxSet)
			return false;
		
		// Create the delta.
		D3DXVECTOR3 vecDelta = vecViewLocation - vecEyePosition;

		//Loop through all hitboxes and see if we can hit one.
		for (int i = 0; i < pHitboxSet->numhitboxes; i++)
		{
			mstudiobbox_t* box = pHitboxSet->pHitbox(i);
			if (!box)
				continue;

			// Scale down hitboxes to ensure a hit (you can remove this, but I prefer having smaller hitboxes to be a little bit more legit :D).
			D3DXVECTOR3 vecMin, vecMax;
			float fScale;

			switch (nScale)
			{
			case 1:
				fScale = 0.8f;
				break;
			case 2:
				fScale = 0.6f;
				break;
			default:
			case 0:
				fScale = 1.0f;
				break;
			}

			D3DXVec3Scale(&vecMin, &box->bbmin, fScale);
			D3DXVec3Scale(&vecMax, &box->bbmax, fScale);

			// Check if we hit the box.
			if (IntersectRayWithOBB(vecEyePosition, vecDelta, matrix[box->bone], vecMin, vecMax))
				return true;
		}

		// We didn't hit anything.
		return false;
	}

	/// <summary>
	/// Fires a shot.
	/// </summary>
	void Shoot()
	{
		// TODO: This should be done in a CreateMove hook. Sending mouse_events is very messy.
		mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		Sleep(10);
		mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
	}
}