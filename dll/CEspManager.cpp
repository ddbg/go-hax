/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CEspManager.h"

extern CExternalDraw * g_pExternalDraw;
extern CSettings * g_pSettings;
extern _RENDER_OBJECTS g_RenderObjects;

namespace CEspManager
{
	/// <summary>
	/// Draws on the window overlay.
	/// </summary>
	void Render()
	{
		// New frame.
		g_pExternalDraw->BeginScene();

		// Make sure that the EntityLoop routine doesn't update anything in the _RENDER_OBJECTS structure 
		// while Render is attempting to draw it onto the overlay.
		EnterCriticalSection(&g_RenderObjects.cs);

		// Render everything in the player vector.
		for (unsigned int i = 0; i < g_RenderObjects.Players.size(); i++)
		{
			bool bDraw = false;
			DWORD dwColorDraw = 0;
			DWORD dwColorEnemy = D3DCOLOR_ARGB(g_pSettings->FontESP.EnemyAlpha, g_pSettings->FontESP.EnemyRed, g_pSettings->FontESP.EnemyGreen, g_pSettings->FontESP.EnemyBlue);
			DWORD dwColorAlly = D3DCOLOR_ARGB(g_pSettings->FontESP.AllyAlpha, g_pSettings->FontESP.AllyRed, g_pSettings->FontESP.AllyGreen, g_pSettings->FontESP.AllyBlue);

			if (g_pSettings->FontESP.Enabled == 1) // Font ESP enabled only for enemies.
			{
				if (g_RenderObjects.Players[i].nTeamID != g_RenderObjects.LocalPlayerTeam)
				{
					dwColorDraw = dwColorEnemy;
					bDraw = true;
				}
				else
					bDraw = false;
			}
			else if (g_pSettings->FontESP.Enabled == 2) // Font ESP enabled for both enemies and allies.
			{
				dwColorDraw = (g_RenderObjects.Players[i].nTeamID == g_RenderObjects.LocalPlayerTeam) ? dwColorAlly : dwColorEnemy;
				bDraw = true;
			}

			if (bDraw) // Should we draw this entity?
			{
				// Allocate a sufficiently large output buffer for the ESP text.
				char szOutputBuf[1024] = { 0 };
				
				if (g_pSettings->FontESP.ShowNames == 1) // Add player name to output.
				{
					strcat_s(szOutputBuf, g_RenderObjects.Players[i].szPlayerName);
					strcat_s(szOutputBuf, " ");
				}

				if (g_pSettings->FontESP.ShowWeapon == 1) // Add weapon name to output.
				{
					bool bAddParentheses = (strlen(szOutputBuf) > 0) ? true : false;

					if (bAddParentheses)
						strcat_s(szOutputBuf, "(");
					
					strcat_s(szOutputBuf, g_RenderObjects.Players[i].szWeaponName);
					
					if (bAddParentheses)
						strcat_s(szOutputBuf, ")");
				}

				if (g_pSettings->FontESP.ShowHealth == 1) // Add health to output.
				{
					char szHP[10] = { 0 };

					_itoa_s(g_RenderObjects.Players[i].nHealth, szHP, sizeof(szHP), 10);

					if (strlen(szOutputBuf) > 0)
						strcat_s(szOutputBuf, "\n");

					strcat_s(szOutputBuf, "HP: ");
					strcat_s(szOutputBuf, szHP);
				}

				// Draw buffer below the entity.
				g_pExternalDraw->DrawText((int)g_RenderObjects.Players[i].vecLeg.x, (int)g_RenderObjects.Players[i].vecLeg.y, dwColorDraw, szOutputBuf);
			}

			bDraw = false;
			dwColorDraw = 0;
			dwColorEnemy = D3DCOLOR_ARGB(g_pSettings->BoxESP.EnemyAlpha, g_pSettings->BoxESP.EnemyRed, g_pSettings->BoxESP.EnemyGreen, g_pSettings->BoxESP.EnemyBlue);
			dwColorAlly = D3DCOLOR_ARGB(g_pSettings->BoxESP.AllyAlpha, g_pSettings->BoxESP.AllyRed, g_pSettings->BoxESP.AllyGreen, g_pSettings->BoxESP.AllyBlue);
			DWORD dwThicknessDraw = 0;
			DWORD dwThicknessEnemy = g_pSettings->BoxESP.EnemyThickness;
			DWORD dwThicknessAlly = g_pSettings->BoxESP.AllyThickness;

			if (g_pSettings->BoxESP.Enabled == 1) // Box ESP enabled only for enemies.
			{
				if (g_RenderObjects.Players[i].nTeamID != g_RenderObjects.LocalPlayerTeam)
				{
					dwColorDraw = dwColorEnemy;
					dwThicknessDraw = dwThicknessEnemy;
					bDraw = true;
				}
				else
					bDraw = false;
			}
			else if (g_pSettings->BoxESP.Enabled == 2) // Box ESP enabled for both enemies and allies.
			{
				if (g_RenderObjects.Players[i].nTeamID == g_RenderObjects.LocalPlayerTeam)
				{
					dwColorDraw = dwColorAlly;
					dwThicknessDraw = dwThicknessAlly;
				}
				else
				{
					dwColorDraw = dwColorEnemy;
					dwThicknessDraw = dwThicknessEnemy;
				}

				bDraw = true;
			}

			if (bDraw) // Should we draw this entity?
			{
				float Height = abs(g_RenderObjects.Players[i].vecLeg.y - g_RenderObjects.Players[i].vecHead.y);
				float Width = Height / 2.0f;

				// Draw box around entity.
				g_pExternalDraw->DrawRectangle((int)(g_RenderObjects.Players[i].vecHead.x - Width / 2), (int)g_RenderObjects.Players[i].vecHead.y, (int)Width, (int)Height, dwThicknessDraw, dwColorDraw);
			}

			// The two bone lists should always be the same size unless a call to IClientEntity->SetupBones failed in CUtils::GetBonePosition.
			// If the two bone lists are not the same size, we shouldn't render any skeleton ESP since we're missing coords.
			if (g_RenderObjects.Players[i].lstVecBoneStart.size() != g_RenderObjects.Players[i].lstVecBoneEnd.size())
				continue;

			bDraw = false;
			dwColorDraw = 0;
			dwColorEnemy = D3DCOLOR_ARGB(g_pSettings->BoneESP.EnemyAlpha, g_pSettings->BoneESP.EnemyRed, g_pSettings->BoneESP.EnemyGreen, g_pSettings->BoneESP.EnemyBlue);
			dwColorAlly = D3DCOLOR_ARGB(g_pSettings->BoneESP.AllyAlpha, g_pSettings->BoneESP.AllyRed, g_pSettings->BoneESP.AllyGreen, g_pSettings->BoneESP.AllyBlue);
			dwThicknessDraw = 0;
			dwThicknessEnemy = g_pSettings->BoneESP.EnemyThickness;
			dwThicknessAlly = g_pSettings->BoneESP.AllyThickness;
			
			if (g_pSettings->BoneESP.Enabled == 1) // Bone ESP enabled only for enemies.
			{
				if (g_RenderObjects.Players[i].nTeamID != g_RenderObjects.LocalPlayerTeam)
				{
					dwColorDraw = dwColorEnemy;
					dwThicknessDraw = dwThicknessEnemy;
					bDraw = true;
				}
				else
					bDraw = false;
			}
			else if (g_pSettings->BoneESP.Enabled == 2) // Bone ESP enabled for both enemies and allies.
			{
				if (g_RenderObjects.Players[i].nTeamID == g_RenderObjects.LocalPlayerTeam)
				{
					dwColorDraw = dwColorAlly;
					dwThicknessDraw = dwThicknessAlly;
				}
				else
				{
					dwColorDraw = dwColorEnemy;
					dwThicknessDraw = dwThicknessEnemy;
				}

				bDraw = true;
			}

			if (bDraw)  // Should we draw this entity?
			{
				for (unsigned int j = 0; j < g_RenderObjects.Players[i].lstVecBoneStart.size(); j++)
				{
					// Draw bone on entity.
					g_pExternalDraw->DrawLine((int)g_RenderObjects.Players[i].lstVecBoneStart[j].x, (int)g_RenderObjects.Players[i].lstVecBoneStart[j].y, (int)g_RenderObjects.Players[i].lstVecBoneEnd[j].x, (int)g_RenderObjects.Players[i].lstVecBoneEnd[j].y, dwThicknessDraw, dwColorDraw);
				}
			}
		}
		
		// Release the acquired lock.
		LeaveCriticalSection(&g_RenderObjects.cs);

		// Display triggerbot state.
		char szTriggerBotText[255];
		strcpy_s(szTriggerBotText, "[go]hax => TriggerBot: ");

		switch (g_pSettings->TriggerBot.State)
		{
		case 0:
			strcat_s(szTriggerBotText, "DISABLED ");
			break;
		case 2:
			strcat_s(szTriggerBotText, "ENABLED (HEADSHOT) ");
			break;
		default:
		case 1:
			strcat_s(szTriggerBotText, "ENABLED (ALL) ");
			break;
		}

		switch (g_pSettings->TriggerBot.Scale)
		{
		case 1:
			strcat_s(szTriggerBotText, "0.8f");
			break;
		case 2:
			strcat_s(szTriggerBotText, "0.6f");
			break;
		default:
		case 0:
			strcat_s(szTriggerBotText, "1.0f");
			break;
		}

		g_pExternalDraw->DrawText(5, 5, D3DCOLOR_ARGB(255, 192, 192, 192), szTriggerBotText);
		
		// Display frame.
		g_pExternalDraw->EndScene();
	}
}