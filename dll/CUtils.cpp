/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CUtils.h"

extern IVEngineClient * g_pEngineClient;
extern IBaseClientDLL * g_pBaseClient;

namespace CUtils
{
	/// <summary>
	/// Finds the location of the needle in the haystack, if it exists.
	/// </summary>
	/// <param name="pBeginHaystack">("The haystack") The starting point of the memory search.</param>
	/// <param name="dwHaystackLength">The length of the memory region.</param>
	/// <param name="pNeedle">("The needle") The search term.</param>
	/// <param name="dwNeedleLength">The length of the search term.</param>
	/// <returns>Returns the address in the haystack where the needle was found or 0 if nothing was found.</returns>
	ULONG_PTR FindPattern(ULONG_PTR pBeginHaystack, DWORD dwHaystackLength, PCHAR pNeedle, DWORD dwNeedleLength)
	{
		// The end of the section...
		ULONG_PTR pEndHaystack = (pBeginHaystack + dwHaystackLength) - dwNeedleLength;

		DWORD dwBytesFound = 0;
		do
		{
			BYTE bHaystack = *(PBYTE)pBeginHaystack;
			BYTE bNeedle = pNeedle[dwBytesFound];
			
			if (bNeedle != '?')
			{
				if (bHaystack == bNeedle)
					dwBytesFound++;
				else
				{
					pBeginHaystack = (pBeginHaystack - ((dwBytesFound == 0) ? dwBytesFound : (dwBytesFound - 1)));
					dwBytesFound = 0;
				}
			}
			else
				dwBytesFound++;

			if (dwBytesFound >= dwNeedleLength)
				return  ((pBeginHaystack - dwBytesFound) + 1);

		} while (pBeginHaystack++ < pEndHaystack);

		return 0;
	}

	/// <summary>
	/// Finds the location of the needle in the haystack, if it exists.
	/// </summary>
	/// <param name="pModuleBase">The module base.</param>
	/// <param name="pszSection">The section in the module where the search term is expected to be found.</param>
	/// <param name="pNeedle">("The needle") The search term.</param>
	/// <param name="dwNeedleLength">The length of the search term.</param>
	/// <returns>Returns the address in the haystack where the needle was found or 0 if nothing was found.</returns>
	ULONG_PTR FindPattern(ULONG_PTR pModuleBase, PCHAR pszSection, PCHAR pNeedle, DWORD dwNeedleLength)
	{
		if (!pModuleBase)
			return 0;

		// Does the module have a valid DOS signature?
		PIMAGE_DOS_HEADER pIDH = (PIMAGE_DOS_HEADER)pModuleBase;
		if (pIDH->e_magic != IMAGE_DOS_SIGNATURE)
			return 0;

		// Does the module have a valid NT signature?
		PIMAGE_NT_HEADERS pINH = (PIMAGE_NT_HEADERS)(pModuleBase + pIDH->e_lfanew);
		if (pINH->Signature != IMAGE_NT_SIGNATURE)
			return 0;

		// Loop through all the sections until we find the target section.
		PIMAGE_SECTION_HEADER pISH = IMAGE_FIRST_SECTION(pINH);

		// Denotes whether or not we found the section.
		bool bFoundSection = false;
		
		for (int i = 0; i < pINH->FileHeader.NumberOfSections; i++)
		{
			if (!strncmp((PCHAR)pISH->Name, pszSection, strlen(pszSection)))
			{
				bFoundSection = true;
				break;
			}

			pISH++;
		}

		// Didn't find the section we were looking for; bail!
		if (!bFoundSection)
			return 0;

		return FindPattern(pModuleBase + pISH->VirtualAddress, pISH->Misc.VirtualSize, pNeedle, dwNeedleLength);
	}

	/// <summary>
	/// Gets the target bone position.
	/// </summary>
	/// <param name="pClientEntity">The client entity.</param>
	/// <param name="vecHitbox">[OUTPUT] The hitbox game coords.</param>
	/// <param name="nTargetBone">The target bone.</param>
	/// <returns>Returns TRUE if the bone's position was successfully retrieved or FALSE if something failed.</returns>
	bool GetBonePosition(IClientEntity* pClientEntity, D3DXVECTOR3& vecHitbox, int nTargetBone)
	{
		matrix3x4_t arrBoneMatrix[128];
		if (!pClientEntity->SetupBones(&arrBoneMatrix[0], 128, 0x00000100, g_pEngineClient->Time()))
			return false;

		vecHitbox = D3DXVECTOR3(arrBoneMatrix[nTargetBone][0][3], arrBoneMatrix[nTargetBone][1][3], arrBoneMatrix[nTargetBone][2][3]);

		return true;
	}

	/// <summary>
	/// Converts game coords to screen cords.
	/// </summary>
	/// <param name="vecOrigin">The game coords.</param>
	/// <param name="vecScreen">[OUTPUT] The screen coords.</param>
	/// <returns>Returns TRUE if the game coords were successfully converted to screen coords or FALSE if something failed (e.g. game coords are not visible on screen).</returns>
	bool WorldToScreen(D3DXVECTOR3 vecOrigin, D3DXVECTOR3 &vecScreen)
	{
		// Get the resolution of the game (it may change at any point).
		int nWidth = 0, nHeight = 0;
		g_pEngineClient->GetScreenSize(nWidth, nHeight);
		
		// Grab the world to screen matrix.
		matrix3x4_t& matWorldToScreen = g_pEngineClient->WorldToScreenMatrix();

		// Calculate the angle in comparison to the player's camera.
		float w = matWorldToScreen[3][0] * vecOrigin[0] + matWorldToScreen[3][1] * vecOrigin[1] + matWorldToScreen[3][2] * vecOrigin[2] + matWorldToScreen[3][3]; 
		
		// Screen doesn't have a 3rd dimension.
		vecScreen.z = 0; 

		if (w > 0.001f) // If the object is within view.
		{
			float fl1DBw = 1 / w; // Divide 1 by the angle.
			vecScreen.x = (float)((nWidth / 2) + (0.5 * ((matWorldToScreen[0][0] * vecOrigin[0] + matWorldToScreen[0][1] * vecOrigin[1] + matWorldToScreen[0][2] * vecOrigin[2] + matWorldToScreen[0][3]) * fl1DBw) * nWidth + 0.5)); // Get the X dimension and push it in to the Vector.
			vecScreen.y = (float)((nHeight / 2) - (0.5 * ((matWorldToScreen[1][0] * vecOrigin[0] + matWorldToScreen[1][1] * vecOrigin[1] + matWorldToScreen[1][2] * vecOrigin[2] + matWorldToScreen[1][3]) * fl1DBw) * nHeight + 0.5)); // Get the Y dimension and push it in to the Vector.
			return true;
		}

		return false;
	}

	/// <summary>
	/// Gets the offset to the net variable.
	/// </summary>
	/// <param name="pszClassName">Name of the class.</param>
	/// <param name="pszVariable">Name of the variable.</param>
	/// <param name="pTable">The table to search in for the target variable.</param>
	/// <returns>Returns the offset of the net variable or 0 if nothing was found.</returns>
	int GetNetVar(PCHAR pszClassName, PCHAR pszVariable, RecvTable * pTable)
	{
		// Bad table pointer; bail.
		if (!pTable)
			return 0;

		// Loop through each property in the table.
		for (int i = 0; i < pTable->GetNumProps(); i++)
		{
			RecvProp * pProp = pTable->GetProp(i);

			// Bad property pointer; bail.
			if (!pProp)
				continue;

			// Did we match our target?
			if (!strcmp(pTable->GetName(), pszClassName) && !strcmp(pProp->GetName(), pszVariable))
				return pProp->GetOffset();

			// Check all subtables within this property...
			int nOffset = GetNetVar(pszClassName, pszVariable, pProp->GetDataTable());
			if (nOffset != 0)
				return nOffset;

		}

		return 0;
	}

	/// <summary>
	/// Gets the offset to the net variable.
	/// </summary>
	/// <param name="pszClassName">Name of the class.</param>
	/// <param name="pszVariable">Name of the variable.</param>
	/// <returns>Returns the offset of the net variable or 0 if nothing was found.</returns>
	int GetNetVar(PCHAR pszClassName, PCHAR pszVariable)
	{
		ClientClass * pClientClass = g_pBaseClient->GetAllClasses();
		
		// Loop until there are no more client classes.
		for (; pClientClass; pClientClass = pClientClass->NextClass())
		{
			int nOffset = GetNetVar(pszClassName, pszVariable, pClientClass->GetTable());
			if (nOffset != 0)
				return nOffset;
		}

		return 0;
	}

	/// <summary>
	/// Outputs a debug message.
	/// </summary>
	/// <param name="nErrorCode">Any non-zero value will cause this routine to display an alert to the user and terminate process execution.</param>
	/// <param name="pszFormat">Format of the string.</param>
	void DebugPrint(int nErrorCode, PCHAR pszFormat, ...)
	{
		va_list args;

		va_start(args, pszFormat);

		// How big would this string be if it was expanded out?
		DWORD dwBufSize = _vscprintf(pszFormat, args) + 1;

		// Allocate enough space...
		PCHAR pszBuf = (PCHAR)malloc(dwBufSize);

		// Write it...
		vsprintf_s(pszBuf, dwBufSize, pszFormat, args);
		OutputDebugString(pszBuf);

		if (nErrorCode != 0)
		{
			MessageBox(0, pszBuf, "[go]hax - Critical Error", MB_ICONERROR | MB_OK);
			ExitProcess(nErrorCode);
		}


		// Free the mallocs!
		free(pszBuf);

		va_end(args);
	}

}