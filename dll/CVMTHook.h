/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <Windows.h>

/// <summary>
/// A generic VMT hook.
/// </summary>
class CVMTHook
{
public:
	CVMTHook(PULONG_PTR pVtable, int nIndex, ULONG_PTR pNewRoutine);
	~CVMTHook();

	/// <summary>
	/// Executes the old routine.
	/// </summary>
	/// <returns>The appropriate return value for the typedef'd function.</returns>
	template< typename Fn >
	Fn Execute()
	{
		return reinterpret_cast<Fn>(m_pOldRoutine);
	}

private:
	PULONG_PTR m_pVtable;
	int m_nIndex;
	ULONG_PTR m_pOldRoutine;
};