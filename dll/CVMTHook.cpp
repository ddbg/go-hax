/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CVMTHook.h"

/// <summary>
/// Initializes a new instance of the <see cref="CVMTHook"/> class. Hooks a VMT.
/// </summary>
/// <param name="pVtable">The pointer to the vtable.</param>
/// <param name="nIndex">The index of the routine to hook.</param>
/// <param name="pNewRoutine">The address of the new routine that will replace the old one.</param>
CVMTHook::CVMTHook(PULONG_PTR pVtable, int nIndex, ULONG_PTR pNewRoutine) : m_pVtable(pVtable), m_nIndex(nIndex)
{
	// Store off the old routine's address in case we ever decide to unhook.
	m_pOldRoutine = pVtable[nIndex];

	// Hook the virtual method.
	DWORD dwOldProtections = 0;

	VirtualProtect(&pVtable[nIndex], sizeof(ULONG_PTR), PAGE_EXECUTE_READWRITE, &dwOldProtections);
	pVtable[nIndex] = pNewRoutine;
	VirtualProtect(&pVtable[nIndex], sizeof(ULONG_PTR), dwOldProtections, &dwOldProtections);
}

/// <summary>
/// Finalizes an instance of the <see cref="CVMTHook"/> class. Unhooks the hooked VMT.
/// </summary>
CVMTHook::~CVMTHook()
{
	// Unhook the virtual method.
	DWORD dwOldProtections = 0;

	VirtualProtect(&m_pVtable[m_nIndex], sizeof(ULONG_PTR), PAGE_EXECUTE_READWRITE, &dwOldProtections);
	m_pVtable[m_nIndex] = m_pOldRoutine;
	VirtualProtect(&m_pVtable[m_nIndex], sizeof(ULONG_PTR), dwOldProtections, &dwOldProtections);
}