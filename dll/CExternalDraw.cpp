/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "CExternalDraw.h"


/// <summary>
/// Initializes a new instance of the <see cref="CExternalDraw"/> class.
/// </summary>
/// <param name="pszClassName">Name of the window class.</param>
/// <param name="pszWindowName">Name of the window.</param>
/// <param name="nPosX">The x position of the new window.</param>
/// <param name="nPosY">The y position of the new window.</param>
/// <param name="nWidth">The width of the window.</param>
/// <param name="nHeight">The height of the window.</param>
/// <param name="pRenderFunction">The pointer to the render function that will be called every tick to display graphics on top of the window overlay.</param>
CExternalDraw::CExternalDraw(PCHAR pszClassName, PCHAR pszWindowName, int nPosX, int nPosY, int nWidth, int nHeight, PVOID pRenderFunction) :
	m_pszClassName(pszClassName), m_pszWindowName(pszWindowName), m_nPosX(nPosX), m_nPosY(nPosY), m_nWidth(nWidth), m_nHeight(nHeight),
	m_pD3DDevice9(NULL), m_pD3D9(NULL), m_pD3DXFont(NULL), m_pD3DXLine(NULL)
{
	m_pRenderFunction = (RenderFunction)pRenderFunction;
	m_hThread = CreateThread(NULL, 0, DummyThread, this, 0, NULL);
}

/// <summary>
/// Finalizes an instance of the <see cref="CExternalDraw"/> class.
/// </summary>
CExternalDraw::~CExternalDraw()
{
	if (m_pD3DXLine)
		m_pD3DXLine->Release();

	if (m_pD3DXFont)
		m_pD3DXFont->Release();

	if (m_pD3DDevice9)
		m_pD3DDevice9->Release();

	if (m_pD3D9)
		m_pD3D9->Release();

	if (m_hWnd)
		DestroyWindow(m_hWnd);

	if (m_hThread)
	{
		TerminateThread(m_hThread, 0);
		CloseHandle(m_hThread);
	}
}

/// <summary>
/// Creates an overlay window and initializes a D3D interface to use for drawing. Also contains the window message loop.
/// </summary>
/// <param name="lpThreadParameter">The thread parameter.</param>
/// <returns>If successful, this method should never return.</returns>
DWORD CExternalDraw::WindowThread(LPVOID lpThreadParameter)
{
	// Create an overlay...
	WNDCLASSEX WndClassEx;
	ZeroMemory(&WndClassEx, sizeof(WNDCLASSEX));

	WndClassEx.cbClsExtra = NULL;
	WndClassEx.cbSize = sizeof(WNDCLASSEX);
	WndClassEx.cbWndExtra = NULL;
	WndClassEx.hCursor = LoadCursor(0, IDC_ARROW);
	WndClassEx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClassEx.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClassEx.style = CS_HREDRAW | CS_VREDRAW;
	WndClassEx.lpfnWndProc = DummyProc;
	WndClassEx.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	WndClassEx.lpszClassName = m_pszClassName;
	if (!RegisterClassEx(&WndClassEx))
		CUtils::DebugPrint(1, "[-] Failed to register window class for overlay.\n");

	m_hWnd = CreateWindowEx(WS_EX_TOPMOST | WS_EX_TRANSPARENT /* WS_EX_TOOLWINDOW */ | WS_EX_LAYERED, m_pszClassName, m_pszWindowName, WS_POPUP, m_nPosX, m_nPosY, m_nWidth, m_nHeight, NULL, NULL, NULL, NULL);
	if (!m_hWnd)
		CUtils::DebugPrint(1, "[-] Failed to create new window for overlay.\n");

	// Associate the window with this class...
	SetWindowLongPtr(m_hWnd, GWLP_USERDATA, (LONG)this);

	// Force overlay to be transparent...
	SetLayeredWindowAttributes(m_hWnd, RGB(0, 0, 0), 255, LWA_COLORKEY | LWA_ALPHA);
	ShowWindow(m_hWnd, SW_SHOW);

	// Initialize a D3D interface.
	if (FAILED(Direct3DCreate9Ex(D3D_SDK_VERSION, &m_pD3D9)))
		CUtils::DebugPrint(1, "[-] Failed to initialize a Direct3D object.\n");

	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = m_hWnd;
	d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;
	d3dpp.BackBufferWidth = m_nWidth;
	d3dpp.BackBufferHeight = m_nHeight;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// Create the appropriate device.
	if (FAILED(m_pD3D9->CreateDeviceEx(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, 0, &m_pD3DDevice9)))
		CUtils::DebugPrint(1, "[-] Failed to create a Direct3D device.\n");

	// Render all fonts in this typeface.
	if (FAILED(D3DXCreateFontA(m_pD3DDevice9, 12, 0, 0, /* FW_BOLD */ 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, "Verdana", &m_pD3DXFont)))
		CUtils::DebugPrint(1, "[-] Failed to create a Direct3D font object.\n");

	// Creating a line interface for later use...
	if (FAILED(D3DXCreateLine(m_pD3DDevice9, &m_pD3DXLine)))
		CUtils::DebugPrint(1, "[-] Failed to create a Direct3D line object.\n");

	// Make sure the overlay is in the foreground.
	SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

	while (TRUE)
	{
		MSG msg = { 0 };
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
			break;
		
		if (m_pRenderFunction)
			m_pRenderFunction();
	}

	/* This should never return. */
	return 0;
}

/// <summary>
/// Processes messages sent to the window.
/// </summary>
/// <param name="hWnd">A handle to the window.</param>
/// <param name="uMessage">The message.</param>
/// <param name="wParam">Additional message information.</param>
/// <param name="lParam">Additional message information.</param>
/// <returns>Depends on the message sent.</returns>
LRESULT CExternalDraw::WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hWnd, uMessage, wParam, lParam);
}

/// <summary>
/// Creates a new frame to render upon.
/// </summary>
/// <param name="bClear">If set to <c>true</c>, clears the previous frame.</param>
void CExternalDraw::BeginScene(bool bClear /* = true */)
{
	if (m_pD3DDevice9)
	{
		if (bClear)
			m_pD3DDevice9->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);

		m_pD3DDevice9->BeginScene();
	}
}

/// <summary>
/// Completes frame render.
/// </summary>
/// <param name="bPresent">If set to <c>true</c>, presents the frame.</param>
void CExternalDraw::EndScene(bool bPresent /* = true */)
{
	if (m_pD3DDevice9)
	{

		m_pD3DDevice9->EndScene();

		if (bPresent)
			m_pD3DDevice9->PresentEx(NULL, NULL, NULL, NULL, NULL);
	}
}

/// <summary>
/// Draws text.
/// </summary>
/// <param name="nPosX">The x-coord.</param>
/// <param name="nPosY">The y-coord.</param>
/// <param name="dwColor">The color of the text.</param>
/// <param name="pszFormat">Format of the string.</param>
void CExternalDraw::DrawText(int nPosX, int nPosY, DWORD dwColor, PCHAR pszFormat, ...)
{
	if (m_pD3DXFont)
	{
		va_list args;
		va_start(args, pszFormat);

		// How big would this string be if it was expanded out?
		DWORD dwBufSize = _vscprintf(pszFormat, args) + 1;

		// Allocate enough space...
		PCHAR pszBuf = (PCHAR)malloc(dwBufSize);

		// Write it...
		vsprintf_s(pszBuf, dwBufSize, pszFormat, args);

		RECT rtFont = { 0 };
		rtFont.left = nPosX;
		rtFont.top = nPosY;
		m_pD3DXFont->DrawTextA(NULL, pszBuf, -1, &rtFont, DT_NOCLIP, dwColor);

		// Free the mallocs!
		free(pszBuf);

		va_end(args);
	}
}

/// <summary>
/// Draws a line.
/// </summary>
/// <param name="nStartX">The starting x-coord.</param>
/// <param name="nStartY">The starting y-coord.</param>
/// <param name="nEndX">The ending x-coord.</param>
/// <param name="nEndY">The ending y-coord.</param>
/// <param name="nThickness">The thickness of the line.</param>
/// <param name="dwColor">The color of the line.</param>
void CExternalDraw::DrawLine(int nStartX, int nStartY, int nEndX, int nEndY, int nThickness, DWORD dwColor)
{
	if (m_pD3DXLine)
	{
		D3DXVECTOR2 points[2];
		points[0].x = (float)nStartX;
		points[0].y = (float)nStartY;
		points[1].x = (float)nEndX;
		points[1].y = (float)nEndY;

		m_pD3DXLine->SetWidth((float)nThickness);
		m_pD3DXLine->Draw(points, 2, dwColor);
	}
}

/// <summary>
/// Draws a rectangle.
/// </summary>
/// <param name="nPosX">The x-coord.</param>
/// <param name="nPosY">The y-coord.</param>
/// <param name="nWidth">The width of the rectangle.</param>
/// <param name="nHeight">The height of the rectangle.</param>
/// <param name="nThickness">The thickness of the rectangle.</param>
/// <param name="dwColor">The outline color of the rectangle.</param>
void CExternalDraw::DrawRectangle(int nPosX, int nPosY, int nWidth, int nHeight, int nThickness, DWORD dwColor)
{
	if (m_pD3DXLine)
	{
		D3DXVECTOR2 points[5];
		points[0] = D3DXVECTOR2((float)nPosX, (float)nPosY);
		points[1] = D3DXVECTOR2((float)nPosX + nWidth, (float)nPosY);
		points[2] = D3DXVECTOR2((float)nPosX + nWidth, (float)nPosY + nHeight);
		points[3] = D3DXVECTOR2((float)nPosX, (float)nPosY + nHeight);
		points[4] = D3DXVECTOR2((float)nPosX, (float)nPosY);
		m_pD3DXLine->SetWidth((float)nThickness);
		m_pD3DXLine->Draw(points, 5, dwColor);
	}
}