/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "main.h"

// DLLs...
HMODULE g_hClient = NULL;
HMODULE g_hEngine = NULL;
HMODULE g_hStudioRender = NULL;
HMODULE g_hShadeRapidx9 = NULL;

// Classes within DLLs...
IBaseClientDLL * g_pBaseClient = NULL;
IClientEntityList * g_pClientEntityList = NULL;
IVEngineClient * g_pEngineClient = NULL;
IVModelInfo * g_pModelInfo = NULL;
IStudioRender * g_pStudioRender = NULL;

// Custom classes...
CExternalDraw * g_pExternalDraw = NULL;
CVMTHook * g_hkBeginFrame = NULL;
CSettings * g_pSettings = NULL;

_NETVAR_OFFSETS g_Offsets = { 0 };
_RENDER_OBJECTS g_RenderObjects = { 0 };

/// <summary>
/// The entry point.
/// </summary>
/// <param name="hinstDLL">A handle to the DLL module. The value is the base address of the DLL.</param>
/// <param name="fdwReason">The reason code that indicates why the DLL entry-point function is being called.</param>
/// <param name="lpvReserved">Reserved.</param>
/// <returns>Returns TRUE if it succeeds or FALSE if initialization fails. </returns>
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
	{
		BOOL bIsAeroEnabled = false;

		if (FAILED(DwmIsCompositionEnabled(&bIsAeroEnabled)) || !bIsAeroEnabled)
		{
			if (MessageBox(0, "[-] Failed to load. Windows Aero is disabled. Please enable Aero and try again.\n\nWould you like instructions on how to enable Aero?", "[go]hax", MB_ICONERROR | MB_YESNO) == IDYES)
				ShellExecute(NULL, "open", "iexplore.exe", "https://www.youtube.com/watch?v=T6xNjfRkMuo", NULL, SW_SHOWNORMAL);

			ExitProcess(1);
		}

		MessageBox(0, "[+] Locked and loaded.\n\nPress OK to continue.", "[go]hax", MB_ICONINFORMATION | MB_OK);
		CreateThread(NULL, 0, Initialize, hinstDLL, 0, NULL);
		break;
	}
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

/// <summary>
/// Initializes important hack data structures, offsets, and queues up subsequent execution.
/// </summary>
/// <param name="hinstDLL">A handle to the DLL module. The value is the base address of the DLL.</param>
/// <returns>Returns an error code on failure. If successful, this method should never return.</returns>
DWORD WINAPI Initialize(PVOID hinstDLL)
{
	CUtils::DebugPrint(0, "[+] Remote thread executing...\n");
	
	// Load up settings for the hack.
	g_pSettings = new CSettings((HMODULE)hinstDLL);

	// Retrieve settings from the INI file or use the default ones if applicable.
	g_pSettings->Get();

	// Find the base address of these modules... we need them later.
	do
	{
		g_hClient = GetModuleHandle("client.dll");
		g_hEngine = GetModuleHandle("engine.dll");
		g_hStudioRender = GetModuleHandle("studiorender.dll");
		g_hShadeRapidx9 = GetModuleHandle("shaderapidx9.dll");
		Sleep(1);
	} while (!g_hClient || !g_hEngine || !g_hStudioRender);

	CUtils::DebugPrint(0, "[+] Acquired handle to client.dll, engine.dll, studiorender.dll, and shaderapidx9.dll.\n");

	CreateInterfaceFn CreateInterfaceClient = (CreateInterfaceFn)GetProcAddress(g_hClient, "CreateInterface");
	if (!CreateInterfaceClient)
		CUtils::DebugPrint(1, "[-] Failed to find CreateInterface in client.dll.\n");

	CreateInterfaceFn CreateInterfaceEngine = (CreateInterfaceFn)GetProcAddress(g_hEngine, "CreateInterface");
	if (!CreateInterfaceEngine)
		CUtils::DebugPrint(1, "[-] Failed to find CreateInterface in engine.dll.\n");
		
	CreateInterfaceFn CreateInterfaceStudioRender = (CreateInterfaceFn)GetProcAddress(g_hStudioRender, "CreateInterface");
	if (!CreateInterfaceStudioRender)
		CUtils::DebugPrint(1, "[-] Failed to find CreateInterface in studiorender.dll.\n");
	
	CUtils::DebugPrint(0, "[+] Retrieved exported CreateInterface address of client.dll, engine.dll, and studiorender.dll.\n");

	g_pEngineClient = (IVEngineClient *)CreateInterfaceEngine(VENGINE_CLIENT_INTERFACE_VERSION_13, NULL);
	if (!g_pEngineClient)
		CUtils::DebugPrint(1, "[-] Couldn't retrieve EngineClient pointer from engine.dll.\n");

	g_pModelInfo = (IVModelInfo *)CreateInterfaceEngine(VMODELINFO_CLIENT_INTERFACE_VERSION, NULL);
	if (!g_pModelInfo)
		CUtils::DebugPrint(1, "[-] Couldn't retrieve ModelInfo pointer from engine.dll.\n");

	g_pBaseClient = (IBaseClientDLL *)CreateInterfaceClient(CLIENT_DLL_INTERFACE_VERSION, NULL);
	if (!g_pBaseClient)
		CUtils::DebugPrint(1, "[-] Couldn't retrieve IBaseClientDll pointer from client.dll.\n");

	g_pClientEntityList = (IClientEntityList *)CreateInterfaceClient(VCLIENTENTITYLIST_INTERFACE_VERSION, NULL);
	if (!g_pClientEntityList)
		CUtils::DebugPrint(1, "[-] Couldn't retrieve IClientEntityList pointer from client.dll.\n");

	g_pStudioRender = (IStudioRender *)CreateInterfaceStudioRender(STUDIO_RENDER_INTERFACE_VERSION, NULL);
	if (!g_pStudioRender)
		CUtils::DebugPrint(1, "[-] Couldn't retrieve IStudioRender pointer from studiorender.dll.\n");

	// Get offsets for net variables...
	g_Offsets.m_hActiveWeapon = CUtils::GetNetVar("DT_BaseCombatCharacter", "m_hActiveWeapon");
	if (!g_Offsets.m_hActiveWeapon)
		CUtils::DebugPrint(1, "[-] Failed to locate player active weapon offset.\n");

	g_Offsets.m_iHealth = CUtils::GetNetVar("DT_BasePlayer", "m_iHealth");
	if (!g_Offsets.m_iHealth)
		CUtils::DebugPrint(1, "[-] Failed to locate player health offset.\n");
	
	g_Offsets.m_lifeState = CUtils::GetNetVar("DT_BasePlayer", "m_lifeState");
	if (!g_Offsets.m_lifeState)
		CUtils::DebugPrint(1, "[-] Failed to locate player life state offset.\n");

	g_Offsets.m_iTeamNum = CUtils::GetNetVar("DT_BaseEntity", "m_iTeamNum");
	if (!g_Offsets.m_iTeamNum)
		CUtils::DebugPrint(1, "[-] Failed to locate player team offset.\n");

	g_Offsets.m_fFlags = CUtils::GetNetVar("DT_BasePlayer", "m_fFlags");
	if (!g_Offsets.m_fFlags)
		CUtils::DebugPrint(1, "[-] Failed to locate player flags offset.\n");

	g_Offsets.m_hOwnerEntity = CUtils::GetNetVar("DT_BaseEntity", "m_hOwnerEntity");
	if (!g_Offsets.m_hOwnerEntity)
		CUtils::DebugPrint(1, "[-] Failed to locate owner entity offset.\n");

	g_Offsets.m_flFlashDuration = CUtils::GetNetVar("DT_CSPlayer", "m_flFlashDuration");
	if (!g_Offsets.m_flFlashDuration)
		CUtils::DebugPrint(1, "[-] Failed to locate flash duration offset.\n");

	g_Offsets.m_flFlashMaxAlpha = CUtils::GetNetVar("DT_CSPlayer", "m_flFlashMaxAlpha");
	if (!g_Offsets.m_flFlashMaxAlpha)
		CUtils::DebugPrint(1, "[-] Failed to locate flash max alpha offset.\n");

	g_Offsets.m_bIsScoped = CUtils::GetNetVar("DT_CSPlayer", "m_bIsScoped");
	if (!g_Offsets.m_bIsScoped)
		CUtils::DebugPrint(1, "[-] Failed to locate scope offset.\n");

	g_Offsets.m_vecViewOffset = CUtils::GetNetVar("DT_LocalPlayerExclusive", "m_vecViewOffset[0]");
	if (!g_Offsets.m_vecViewOffset)
		CUtils::DebugPrint(1, "[-] Failed to locate eye angles offset.\n");

	g_Offsets.m_CurrentStage = CUtils::GetNetVar("DT_ParticleSmokeGrenade", "m_CurrentStage");
	if (!g_Offsets.m_CurrentStage)
		CUtils::DebugPrint(1, "[-] Failed to locate current state offset.\n");

	g_Offsets.m_nHitboxSet = CUtils::GetNetVar("DT_BaseAnimating", "m_nHitboxSet");
	if (!g_Offsets.m_nHitboxSet)
		CUtils::DebugPrint(1, "[-] Failed to locate hitbox offset.\n");

	// Calculate where UTIL_TraceLine is in client.dll.
	//		Step 1: Locate the address of the 'RightFoot' string in the .rdata section.
	ULONG_PTR pRightFoot = CUtils::FindPattern((ULONG_PTR)g_hClient, ".rdata", "RightFoot", 9);
	if (!pRightFoot)
		CUtils::DebugPrint(1, "[-] Failed to locate address of string \"RightFoot\".\n");

	//		Step 2:	Find where the address of the 'RightFoot' string is xref'd in the .text section.
	ULONG_PTR pRefRightFoot = CUtils::FindPattern((ULONG_PTR)g_hClient, ".text", (PCHAR)&pRightFoot, sizeof(ULONG_PTR));
	if (!pRefRightFoot)
		CUtils::DebugPrint(1, "[-] Failed to locate xref of \"RightFoot\".\n");
	
	//		Step 3: Locate the `0x600400B` mask. This value is pushed to the UTIL_TraceLine close to the 'RightFoot' xref.
	DWORD dwMaskTraceLine = 0x600400B;
	ULONG_PTR pMaskTraceLine = CUtils::FindPattern(pRefRightFoot, 0x200, (PCHAR)&dwMaskTraceLine, sizeof(DWORD));
	if (!pMaskTraceLine)
		CUtils::DebugPrint(1, "[-] Failed to find mask-value 0x600400B near 0x%p.\n", pRefRightFoot);

	//		Step 4: Find where the next 'call'-opcode is.
	ULONG_PTR pCallTraceLine = CUtils::FindPattern(pMaskTraceLine, 0x40, "\xE8", 1);
	if (!pCallTraceLine)
		CUtils::DebugPrint(1, "[-] Failed to find call-opcode near 0x%p.\n", pMaskTraceLine);
	
	//		Step 5: Yank out the relative address of the call.
	DWORD pRelativeCallOffset = *(PDWORD)(pCallTraceLine + 1);

	//		Step 6: Calculate the absolute address.
	ULONG_PTR pAbsoluteCall = (pCallTraceLine + 5) + pRelativeCallOffset;

	//		Step 7: Calculate offset relative to image base.
	g_Offsets.o_Util_TraceLine = pAbsoluteCall - (ULONG_PTR)g_hClient;

	CUtils::DebugPrint(0,
		"[+] Successfully yanked offsets from CS:GO...\n"
		"\t- DT_BaseCombatCharacter->m_hActiveWeapon = 0x%p\n"
		"\t- DT_BasePlayer->m_iHealth = 0x%p\n" 
		"\t- DT_BasePlayer->m_lifeState = 0x%p\n" 
		"\t- DT_BaseEntity->m_iTeamNum = 0x%p\n"
		"\t- DT_BasePlayer->m_fFlags = 0x%p\n"
		"\t- DT_BaseEntity->m_hOwnerEntity = 0x%p\n" 
		"\t- DT_CSPlayer->m_flFlashDuration = 0x%p\n" 
		"\t- DT_CSPlayer->m_flFlashMaxAlpha = 0x%p\n" 
		"\t- DT_CSPlayer->m_bIsScoped = 0x%p\n"
		"\t- DT_LocalPlayerExclusive->m_vecViewOffset[0] = 0x%p\n"
		"\t- DT_ParticleSmokeGrenade->m_CurrentStage = 0x%p\n"
		"\t- DT_BaseAnimating->m_nHitboxSet = 0x%p\n\n"
		"\t- UTIL_TraceLine = 0x%p\n",
		g_Offsets.m_hActiveWeapon,
		g_Offsets.m_iHealth,
		g_Offsets.m_lifeState,
		g_Offsets.m_iTeamNum,
		g_Offsets.m_fFlags,
		g_Offsets.m_hOwnerEntity,
		g_Offsets.m_flFlashDuration,
		g_Offsets.m_flFlashMaxAlpha,
		g_Offsets.m_bIsScoped,
		g_Offsets.m_vecViewOffset,
		g_Offsets.m_CurrentStage,
		g_Offsets.m_nHitboxSet,
		g_Offsets.o_Util_TraceLine);

	InitializeCriticalSection(&g_RenderObjects.cs);

	// Get handle to desktop window.
	HWND hWnd = GetDesktopWindow();
	if (!hWnd)
		CUtils::DebugPrint(1, "[-] Failed to find desktop window.\n");

	// Get the size of the desktop.
	RECT rectResolution = { 0 };
	if (!GetWindowRect(hWnd, &rectResolution))
		CUtils::DebugPrint(1, "[-] Failed to retrieve desktop window resolution.\n");

	// Create a new window to draw atop of. It shall be the size of the entire desktop.
	g_pExternalDraw = new CExternalDraw(HACK_OVERLAY_NAME, HACK_OVERLAY_NAME, rectResolution.left, rectResolution.top, rectResolution.right, rectResolution.bottom, CEspManager::Render);

	// Hook BeginFrame to gain execution in game context.
	g_hkBeginFrame = new CVMTHook((PULONG_PTR)Vtable(g_pStudioRender), 8, (ULONG_PTR)EntityLoop);

	// Keyboard event loop.
	while (TRUE)
	{
		if (GetAsyncKeyState(VK_NUMPAD1) & 1)
			g_pSettings->TriggerBot.State = 0;
		else if (GetAsyncKeyState(VK_NUMPAD2) & 1)
			g_pSettings->TriggerBot.State = 1;
		else if (GetAsyncKeyState(VK_NUMPAD3) & 1)
			g_pSettings->TriggerBot.State = 2;
		else if (GetAsyncKeyState(VK_NUMPAD4) & 1)
			g_pSettings->TriggerBot.Scale = 0;
		else if (GetAsyncKeyState(VK_NUMPAD5) & 1)
			g_pSettings->TriggerBot.Scale = 1;
		else if (GetAsyncKeyState(VK_NUMPAD6) & 1)
			g_pSettings->TriggerBot.Scale = 2;

		Sleep(1);
	}

	/* This should never return. */
	return 0;
}

/// <summary>
/// Hijacked execution of IStudioRender->BeginFrame via VMT hook. Accesses objects/entities in
/// the context of a game thread. This guarantees that everything is in a 'valid' state before we 
/// attempt to touch it.
/// </summary>
/// <param name="pThis">The pointer to the class. Stored in register ecx and required for __thiscall.</param>
/// <param name="pNotUsed">Unused. Artifact of the function defined as a __fastcall. Stored in edx.</param>
void __fastcall EntityLoop(PVOID pThis, PVOID pNotUsed)
{
	EnterCriticalSection(&g_RenderObjects.cs);
	g_RenderObjects.Players.clear();

	// Only loop through entities if the local client is connected to a game.
	if (!g_pEngineClient->IsDrawingLoadingImage() && g_pEngineClient->IsInGame() && g_pEngineClient->IsConnected() && !g_pEngineClient->Con_IsVisible()) // TODO: Add a check to disable ESP when window focus is not on CS:GO.
	{
		int nLocalPlayerId = g_pEngineClient->GetLocalPlayer();

		// If there is a valid local player ID.
		if (nLocalPlayerId)
		{
			// Attempt to retrieve the client entity from the local player ID.
			IClientEntity * pLocalClient = g_pClientEntityList->GetClientEntity(nLocalPlayerId);

			if (pLocalClient)
			{
				// Store this off for team-checks later...
				g_RenderObjects.LocalPlayerTeam = pLocalClient->GetTeam();

				// Loop through all entities in the list.
				for (int i = 0; i < g_pClientEntityList->GetHighestEntityIndex(); i++)
				{
					IClientEntity * pClientEntity = g_pClientEntityList->GetClientEntity(i);
					if (!pClientEntity)
						continue;

					// We don't care about ourselves.
					if (pLocalClient == pClientEntity)
						continue;

					// We don't care about dormant entities.
					if (pClientEntity->IsDormant())
						continue;

					// Retrieve the player info for the entity (name).
					player_info_t pi;
					ZeroMemory(&pi, sizeof(player_info_t));

					if (!g_pEngineClient->GetPlayerInfo(i, &pi))
						continue;

					// We don't care about non-living players.
					if (pClientEntity->GetLifeState() != LIFE_ALIVE)
						continue;

					PCHAR pszWeapon = NULL;

					// Retrieve the player's active weapon.
					C_BaseCombatWeapon * pActiveCombatWeapon = pClientEntity->GetActiveWeapon(g_pClientEntityList);
					if (pActiveCombatWeapon)
					{
						pszWeapon = pActiveCombatWeapon->GetName();

						// If the weapon name was retrieved, sanitize it by removing the extra crud in the beginning.
						if (pszWeapon)
						{
							pszWeapon = strstr(pszWeapon, "weapon_");

							// Substring exists; remove it.
							if (pszWeapon)
								pszWeapon += 7;
						}

					}

					// Get leg position.
					D3DXVECTOR3 vecLeg = pClientEntity->GetAbsOrigin();
					D3DXVECTOR3 vecScreenLeg;

					// Convert game coords to screen coords.
					if (!CUtils::WorldToScreen(vecLeg, vecScreenLeg))
						continue;

					// Get head position.
					D3DXVECTOR3 vecHead;
					D3DXVECTOR3 vecScreenHead;

					if (!CUtils::GetBonePosition(pClientEntity, vecHead, 10))
						continue;

					// Convert game coords to screen coords.
					if (!CUtils::WorldToScreen(vecHead, vecScreenHead))
						continue;

					byte arrBoneLegL[] = { 28, 27, 26, 1 };
					byte arrBoneLegR[] = { 25, 24, 23, 1 };
					byte arrBoneBody[] = { 5, 4, 3, 2, 1 };
					byte arrBoneArmL[] = { 9, 8, 7, 5 };
					byte arrBoneArmR[] = { 15, 14, 13, 5 };

					D3DXVECTOR3 vecBoneStart;
					D3DXVECTOR3 vecBoneEnd;
					D3DXVECTOR3 vecScreenBoneStart;
					D3DXVECTOR3 vecScreenBoneEnd;

					// Add player to render vector.
					RenderPlayer rp;
#pragma region BoneESP
					for (int i = 0; i < 4; i++)
					{
#pragma region BoneBody
						if (!CUtils::GetBonePosition(pClientEntity, vecBoneStart, arrBoneBody[i]))
							continue;

						if (!CUtils::GetBonePosition(pClientEntity, vecBoneEnd, arrBoneBody[i + 1]))
							continue;

						if (!CUtils::WorldToScreen(vecBoneStart, vecScreenBoneStart))
							continue;

						if (!CUtils::WorldToScreen(vecBoneEnd, vecScreenBoneEnd))
							continue;

						rp.lstVecBoneStart.push_back(vecScreenBoneStart);
						rp.lstVecBoneEnd.push_back(vecScreenBoneEnd);
#pragma endregion
					}

					for (int i = 0; i < 3; i++)
					{
#pragma region BoneLeftLeg
						if (!CUtils::GetBonePosition(pClientEntity, vecBoneStart, arrBoneLegL[i]))
							continue;

						if (!CUtils::GetBonePosition(pClientEntity, vecBoneEnd, arrBoneLegL[i + 1]))
							continue;

						if (!CUtils::WorldToScreen(vecBoneStart, vecScreenBoneStart))
							continue;

						if (!CUtils::WorldToScreen(vecBoneEnd, vecScreenBoneEnd))
							continue;

						rp.lstVecBoneStart.push_back(vecScreenBoneStart);
						rp.lstVecBoneEnd.push_back(vecScreenBoneEnd);
#pragma endregion

#pragma region BoneRightLeg
						if (!CUtils::GetBonePosition(pClientEntity, vecBoneStart, arrBoneLegR[i]))
							continue;

						if (!CUtils::GetBonePosition(pClientEntity, vecBoneEnd, arrBoneLegR[i + 1]))
							continue;

						if (!CUtils::WorldToScreen(vecBoneStart, vecScreenBoneStart))
							continue;

						if (!CUtils::WorldToScreen(vecBoneEnd, vecScreenBoneEnd))
							continue;

						rp.lstVecBoneStart.push_back(vecScreenBoneStart);
						rp.lstVecBoneEnd.push_back(vecScreenBoneEnd);
#pragma endregion

#pragma region BoneLeftArm
						if (!CUtils::GetBonePosition(pClientEntity, vecBoneStart, arrBoneArmL[i]))
							continue;

						if (!CUtils::GetBonePosition(pClientEntity, vecBoneEnd, arrBoneArmL[i + 1]))
							continue;

						if (!CUtils::WorldToScreen(vecBoneStart, vecScreenBoneStart))
							continue;

						if (!CUtils::WorldToScreen(vecBoneEnd, vecScreenBoneEnd))
							continue;

						rp.lstVecBoneStart.push_back(vecScreenBoneStart);
						rp.lstVecBoneEnd.push_back(vecScreenBoneEnd);
#pragma endregion

#pragma region BoneRightArm
						if (!CUtils::GetBonePosition(pClientEntity, vecBoneStart, arrBoneArmR[i]))
							continue;

						if (!CUtils::GetBonePosition(pClientEntity, vecBoneEnd, arrBoneArmR[i + 1]))
							continue;

						if (!CUtils::WorldToScreen(vecBoneStart, vecScreenBoneStart))
							continue;

						if (!CUtils::WorldToScreen(vecBoneEnd, vecScreenBoneEnd))
							continue;

						rp.lstVecBoneStart.push_back(vecScreenBoneStart);
						rp.lstVecBoneEnd.push_back(vecScreenBoneEnd);
#pragma endregion
					}
#pragma endregion
					rp.vecHead = vecScreenHead;
					rp.vecLeg = vecScreenLeg;
					strcpy_s(rp.szWeaponName, ((pszWeapon != NULL) ? pszWeapon : "n/a"));
					strcpy_s(rp.szPlayerName, pi.name);
					rp.nHealth = pClientEntity->GetHealth();
					rp.nTeamID = pClientEntity->GetTeam();

					g_RenderObjects.Players.push_back(rp);
				}

				// If the triggerbot is enabled, check to see if we should hit something (cursor is on top of a player entity)...
				if (g_pSettings->TriggerBot.State && CAimbot::Trigger(pLocalClient, ((g_pSettings->TriggerBot.State == 2) ? true : false), g_pSettings->TriggerBot.Scale))
				{
					BOOL bFire = true;

					// Retrieve the equipped weapon.
					C_BaseCombatWeapon * pActiveCombatWeapon = pLocalClient->GetActiveWeapon(g_pClientEntityList);
					if (pActiveCombatWeapon)
					{
						// TODO: There has to be a better way of checking weapon types; filtering by name is silly.
						PCHAR pszWeapon = pActiveCombatWeapon->GetName();
						if (pszWeapon)
						{
							// If the weapon is a sniper (awp, scout, or auto), only triggerbot if we're scoped... otherwise it's too inaccurate.
							if (strstr(pszWeapon, "awp") || strstr(pszWeapon, "ssg08") || strstr(pszWeapon, "g3sg1") || strstr(pszWeapon, "ssg08"))
							{
								if (!pLocalClient->IsScoped())
									bFire = false;
							}
							else if (strstr(pszWeapon, "knife")) // Never triggerbot on a knife.
								bFire = false;

							// Pew-pew, die!
							if (bFire)
								CAimbot::Shoot();
						}
					}
				}
			}
		}
	}

	LeaveCriticalSection(&g_RenderObjects.cs);

	// Call the original method...
	typedef void(__thiscall* _BeginFrame)(PVOID);
	g_hkBeginFrame->Execute<_BeginFrame>()(pThis);
}
