/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <Windows.h>
#include <vector>

#include "CExternalDraw.h"
#include "CSettings.h"
#include "sdk/source_sdk.h"

/// <summary>
/// Interface to ESP related functionality.
/// </summary>
namespace CEspManager
{
	void Render();
}