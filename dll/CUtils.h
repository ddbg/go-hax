/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <Windows.h>
#include <d3d9.h>
#include <D3dx9core.h>

#include "sdk/source_sdk.h"

/// <summary>
/// Interface to game-engine related functionality.
/// </summary>
namespace CUtils
{
	bool GetBonePosition(IClientEntity* pClientEntity, D3DXVECTOR3& vecHitbox, int nTargetBone);
	bool WorldToScreen(D3DXVECTOR3 vecOrigin, D3DXVECTOR3 &vecScreen);
	int GetNetVar(PCHAR pszClassName, PCHAR pszVariable);
	void DebugPrint(int nErrorCode, PCHAR pszFormat, ...);
	ULONG_PTR FindPattern(ULONG_PTR pModuleBase, PCHAR pszSection, PCHAR pNeedle, DWORD dwNeedleLength);
	ULONG_PTR FindPattern(ULONG_PTR pBeginHaystack, DWORD dwHaystackLength, PCHAR pNeedle, DWORD dwNeedleLength);
}