/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#include <Windows.h>

/// <summary>
/// A settings object for the hack.
/// </summary>
class CSettings
{
public:
	CSettings(HMODULE hLibrary);
	~CSettings();
	void Get();


	struct _TRIGGERBOT
	{
		int State;
		int Scale;
	} TriggerBot;

	struct _FONTESP
	{
		int Enabled;
		int ShowNames;
		int ShowWeapon;
		int ShowHealth;
		int EnemyAlpha;
		int AllyAlpha;
		int EnemyRed;
		int EnemyGreen;
		int EnemyBlue;
		int AllyRed;
		int AllyGreen;
		int AllyBlue;
	} FontESP;

	struct _BONEESP_BOXESP
	{
		int Enabled;
		int EnemyAlpha;
		int AllyAlpha;
		int EnemyThickness;
		int AllyThickness;
		int EnemyRed;
		int EnemyGreen;
		int EnemyBlue;
		int AllyRed;
		int AllyGreen;
		int AllyBlue;
	} BoneESP, BoxESP;

private:
	HANDLE m_hFile;
	char m_szFileName[MAX_PATH];

};