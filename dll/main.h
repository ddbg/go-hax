/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#define HACK_OVERLAY_NAME "MyOverlay"

#include <Windows.h>

#include "sdk/source_sdk.h"
#include "CUtils.h"
#include "CSettings.h"
#include "CVMTHook.h"
#include "CExternalDraw.h"
#include "CEspManager.h"
#include "CAimbot.h"

DWORD WINAPI Initialize(PVOID hinstDLL);
void __fastcall EntityLoop(PVOID pThis, PVOID pNotUsed);
