/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#include "main.h"

/// <summary>
/// The entry point.
/// </summary>
/// <param name="argc">The number of arguments.</param>
/// <param name="argv">The argument array.</param>
/// <returns>Returns an exit code.</returns>
int main(int argc, char ** argv)
{
	std::cout << "[go]hax locked and loaded\n" << std::endl;
	if (argc != 2)
	{
		std::cerr << "[-] Invalid arguments passed in to the application. Syntax:\n" <<
					"\tapplication.exe [FULL PATH OF DLL TO INJECT]" << std::endl;
		goto error;
	}

	PCHAR pszDllPath = argv[1];

	// Quick check to see if the DLL exists.
	HANDLE hFile = CreateFile(pszDllPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		std::cerr << "[-] \"" << pszDllPath << "\" could not be opened. Does it exist? Do you have the appropriate privileges?" << std::endl;
		goto error;
	}
	CloseHandle(hFile);

	DWORD dwDllPathLength = (strlen(pszDllPath) + 1);
	std::cout << "[!] Searching for the \"" << WINDOW_TITLE_GAME << "\" window...";
	
	// Find an instance of the CS:GO game...
	HWND hWndGame = NULL;
	while ((hWndGame = FindWindow(NULL, WINDOW_TITLE_GAME)) == NULL) // TODO: Maybe search via window class name (Valve001) as opposed to window title? 
	{
		std::cout << ".";
		Sleep(1000);
	}

	std::cout << std::endl;
	std::cout << "[+] Found the CS:GO window successfully." << std::endl;

	// Retrieve the PID of the game...
	DWORD dwPidGame = 0;
	GetWindowThreadProcessId(hWndGame, &dwPidGame);
	if (!dwPidGame)
	{
		std::cerr << "[-] Failed to resolve the HWND, " << hWndGame << ", to PID." << std::endl;
		goto error;
	}

	std::cout << "[+] Retrieved PID of CS:GO (" << dwPidGame << ")." << std::endl;
	if (!InjectDll(dwPidGame, pszDllPath, dwDllPathLength))
	{
		std::cerr << "[-] Injection failed." << std::endl;
		goto error;
	}
	
	return 0;
	
error:
	// Allow user to read error before terminating.
	std::cin.get();
	return 1;
}

/// <summary>
/// Injects a DLL into a process.
/// </summary>
/// <param name="dwPid">The PID of the target process.</param>
/// <param name="pszDllPath">The path to the DLL.</param>
/// <param name="dwDllPathLength">The length of the DLL path string.</param>
/// <returns>Returns TRUE if the DLL was injected successfully into the remote process or FALSE if injection failed.</returns>
bool InjectDll(DWORD dwPid, char * pszDllPath, DWORD dwDllPathLength)
{
	HANDLE hProcess = OpenProcess(GENERIC_ALL, FALSE, dwPid);
	if (!hProcess)
	{
		std::cerr << "[-] Couldn't open a handle to the process. Are you running this program as Administrator?" << std::endl;
		return false;
	}

	std::cout << "[+] Opened handle, " << hProcess << ", to process with full access rights." << std::endl;

	PVOID pMemoryAllocated = VirtualAllocEx(hProcess, NULL, dwDllPathLength, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	if (!pMemoryAllocated)
	{
		std::cerr << "[-] Failed to allocate space for DLL path in process." << std::endl;
		return false;
	}

	std::cout << "[+] Allocated " << dwDllPathLength << " bytes of memory for DLL path in process. Buffer at 0x" << std::hex << pMemoryAllocated << "." << std::endl;

	DWORD dwBytesWritten = 0;
	if (!WriteProcessMemory(hProcess, pMemoryAllocated, pszDllPath, dwDllPathLength, &dwBytesWritten) || dwBytesWritten == 0)
	{
		std::cerr << "[-] Failed to write DLL path in process." << std::endl;
		return false;
	}

	std::cout << "[+] Wrote \"" << pszDllPath << "\" to process." << std::endl;

	HMODULE hKernel32 = GetModuleHandle("kernel32.dll");
	if (!hKernel32)
	{
		std::cerr << "[-] Couldn't acquire handle to kernel32.dll." << std::endl;
		return false;
	}

	FARPROC hLoadLibrary = GetProcAddress(hKernel32, "LoadLibraryA");
	if (!hLoadLibrary)
	{
		std::cerr << "[-] Couldn't acquire handle to LoadLibraryA in kernel32.dll." << std::endl;
		return false;
	}

	// IDEA: Manually map the DLL as opposed to using LoadLibrary... It'll produce less artifacts.
	HANDLE hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)hLoadLibrary, pMemoryAllocated, 0, NULL);
	if (!hThread)
	{
		std::cerr << "[-] Failed to create remote thread in process to run injected DLL." << std::endl;
		return false;
	}

	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);

	std::cout << "[+] DLL injected successfully into process. Remote thread spawned." << std::endl;

	VirtualFreeEx(hProcess, pMemoryAllocated, 0, MEM_RELEASE);
	CloseHandle(hProcess);

	return true;
}