/*
*	[go]hax - https://bitbucket.org/devdebug/go-hax/
*		@author: devdebug
*		@contact: ddebug@ymail.com
*/

#pragma once

#define WINDOW_TITLE_GAME "Counter-Strike: Global Offensive"

#include <Windows.h>
#include <iostream>

bool InjectDll(DWORD dwPid, char * pszDllPath, DWORD dwDllPathLength);